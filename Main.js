import Express from 'express';
import Morgan from 'morgan';
import Nunjucks from 'nunjucks';
import BodyParser from 'body-parser';
import Session  from 'express-session';
import Mongoose from 'mongoose';
import { register } from 'express-decorators';
import Favicon from 'serve-favicon';
import Recaptcha from 'express-recaptcha';
import Readline  from 'readline';
import CookieParser from 'cookie-parser';
import FileUpload from 'express-fileupload';

import PagesController from './controllers/PagesController';
import SocketController from './controllers/SocketController';
import UsersController from './controllers/UsersController';
import Config from './config/Config';

const FileStore = require('session-file-store')(Session);


const app = Express();
const session = Session({
    name: 'pasa-session-cookie-id',
    secret: Config.COOKIE,
    saveUninitialized: true,
    resave: false,
    cookie: {maxAge: 43200000},
    store: new FileStore({ttl:18000})
});
const rl = Readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const body = BodyParser.urlencoded({
    extended: true,
    limit: '5mb'
});

app.use('/webroot', Express.static('webroot'));
app.use(Morgan('dev'));
app.use(CookieParser());
app.use(BodyParser.json({limit: '5mb'}) );
app.use(Favicon('./webroot/img/favicon.png'));
app.use(session);
app.use(FileUpload());
app.use(body);
// app.use(function(req, res, next) {
//     res.status(400);
//     res.send('404: File Not Found');
// });
Recaptcha.init(Config.CAPTCHA.SITE_KEY, Config.CAPTCHA.SECRET_KEY);

Mongoose.connect('mongodb://localhost/' + Config.DATABASE);
const db = Mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connection established to bdd');
});
Nunjucks.configure('./view', {
    autoescape: true,
    express: app
});

register(app, new PagesController(db));
register(app, new UsersController(db));

const http = require('http');
const server = http.createServer(app);
const io = require('socket.io').listen(server);
// io.use(SharedSession(session));
io.use(function(socket, next) {
    let req = socket.handshake;
    req.originalUrl = '/'; //Whatever your cookie path is
    // console.log(req);
    session(req, {}, next);
});
const sc = new SocketController(db, io);
io.on('connection', (socket) => {
    socket.on('AUTH', (msg) => {
        sc.login(socket, msg);
    });
    if (socket.handshake.session.user) {
        console.log(socket.handshake.session.user.username + ' connected');
    }
});

rl.on('line', (input) => {
    let decomposed = input.split(' ');
    const cmd = decomposed[0];
    decomposed.shift();
    decomposed = decomposed.filter(v=>v!=='');
    const args = decomposed.length > 0 ? decomposed : [];

    if (cmd === 'sockets') {
        const clients = io.sockets.clients();
        console.log(clients);
    }

    if (cmd === 'broadcast' || cmd === 'bc') {
        console.log(args);
        io.sockets.emit('BROADCAST_MSG', args.join(' '));
    }

    if(cmd === 'stop'){
        process.exit(1);
    }

    console.log(`Received: ${input}`);
});

server.listen(Config.PORT, () => {
    console.log('Server started on port ' + Config.PORT);
});
