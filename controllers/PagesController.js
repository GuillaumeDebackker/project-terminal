/**
 * Created by Anonymous on 12/05/2017.
 */
import { basePath as BasePath, get as Get, use as Use} from 'express-decorators';
import Controller from '../core/Controller';

@BasePath('/')
export default class PagesController extends Controller{
    constructor(db) {
        super(db);
    }

    @Use()
    isAuthenticated(req, res, next) {
        this.layout = req.session.user ? 'user' : 'default';
        return next();
    }

    @Get('/')
    indexGlobal(req, res) {
        this.render(req, res, 'index');
    }
}
