/**
 * Created by Anonymous on 27/05/2017.
 */
import Controller from '../core/Controller';
import UserSchem from '../model/User';
import Config from '../config/Config';
import CookieParser from 'cookie-parser';
import Fs from 'fs';

export default class SocketController extends Controller {

    constructor(db, io){
        super(db);
        this.io = io;
    }

    login(socket, msg) {
        const User = this.db.model('user', UserSchem);

        if (!socket.handshake.session.user) {
            const decodeSess = CookieParser.signedCookie(decodeURIComponent(msg), Config.COOKIE);

            Fs.exists('./sessions/' + decodeSess + '.json', (exists) => {
                if (!exists) {
                    // socket.emit('CONNECT_FAILED', 'redirection...');
                    socket.emit('REDIRECT', '/');
                    return;
                }
                const Session = require('../sessions/' + decodeSess + '.json');
                socket.handshake.session.user = Session.user;
                console.log('Reconnect ' + Session.user.username);
            });
        }
    }
}