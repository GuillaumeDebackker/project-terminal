import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import Config from '../config/Config';
import UserSchem from '../model/User';
import Crypto from 'crypto';
import { homedir } from 'os';

@BasePath('/client')
export default class UsersController extends Controller{
    constructor(db) {
        super(db);
    }

    @Use()
    isAuthenticated(req, res, next) {
        this.layout = req.session.user ? 'user' : 'default';
        return next();
    }

    @Get('/home')
    home(req,res){
        
    }
}    