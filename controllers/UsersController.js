import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import Config from '../config/Config';
import UserSchem from '../model/User';
import Crypto from 'crypto';
import Recaptcha from 'express-recaptcha';

@BasePath('/users')
export default class UsersController extends Controller{
    constructor(db) {
        super(db);
    }

    @Use()
    isAuthenticated(req, res, next) {
        this.layout = req.session.user ? 'user' : 'default';
        return next();
    }

    @Get('/register')
    register(req, res) {
        this.vars['key_site'] = Config.CAPTCHA.SITE_KEY;
        this.render(req, res, 'register');
    }

    @Get('/login')
    login(req,res){
        if(req.session.user)
            res.redirect
        this.render(req, res, 'login');
    }

    @Get('/logout')
    logout(req, res){
        req.session.user = undefined;
        req.session.save();
        this.setFlash(req,'Vous êtes déconnecter');
        res.redirect('/');
    }

    @Post('/loginuser')
    loginuser(req,res){
        const { login, password } =  req.body;
        const User = this.db.model('user', UserSchem);
        const passwordHash = Crypto.createHash('sha256').update(password + login).digest('hex');
        const query = User.findOne({
            username: login,
            password: passwordHash
        });

        query.exec((err, user) =>{
            if (user) {
                req.session.user = user;
                req.session.save();
                this.setFlash(req, 'Vous êtes connectés');
                res.redirect('/');
            }else{
                this.setFlash(req, 'Login / Mot de passe invalide');
                res.redirect('/users/login');
            }
        }).catch((err) => {
            console.log(err);
            res.redirect('/users/login');
        });
    }

    @Post('/useradd')
    useradd(req,res){
        const { username, email, password, confpassword } =  req.body;
        const User = this.db.model('user', UserSchem);

        Recaptcha.verify(req, (err, data) => {
            if(err){
                this.setFlash(req, 'Erreur validation captcha');
                res.redirect('/users/register');
                return true;
            }
            if (password.length < 7) {
                this.setFlash(req, 'Mots de passe trop court', 'danger');
                res.redirect('/users/register');
                return true;
            }

            if (password !== confpassword) {
                this.setFlash(req, 'Mots de passe non identique', 'danger');
                res.redirect('/users/register');
                return true;
            }

            const passwordHash = Crypto.createHash('sha256').update(password + username).digest('hex');

            const userRequest = new User({
                username,
                password: passwordHash,
                email,
                role: 'user'
            });

            const error = userRequest.validateSync();

            if (error) {
                const errors = this.extartErrors(error.errors);
                console.log(errors);
                this.setFlash(req, errors, 'danger');
                res.redirect('/users/register');
                return true;
            }

            userRequest.save((err) => {
                if (err) {
                    if (11000 === err.code || 11001 === err.code) {
                        this.setFlash(req, 'Username / Email exitant déjà');
                        res.redirect('/users/register');
                        return true;
                    }
                }

                this.setFlash(req, 'Compte créé');
                res.redirect('/');
                return true;
            });
        });
    }
}    