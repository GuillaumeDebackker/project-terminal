var Load = Class.extend(function () {

    var workspace = new WorkspacePanel();
    var workspaceOption = new WorkspaceOptPanel();
    var u = new Util();

    this.loadLauncher = function (value) {
        //TODO
        // data = new Util().readJSON(RES + 'load.php?id=' + ID + '&token=' + TOKEN);
        // data = new Util().readJSON(RES + 'kelix.json');

        var data = '';
        var theme = !(IDTHEME == 0);

        if (IDTHEME == 0) {
            // data = new Util().readJSON(RES + 'load.php?id=' + ID + '&token=' + TOKEN);
            data = new Util().readJSON('/editor/load/' + TOKEN);
        } else {
            // data = new Util().readJSON(RES + 'load.php?id=' + ID + '&token=' + TOKEN + '&theme=' + IDTHEME);
            data = new Util().readJSON('/editor/load/' + TOKEN);
        }
        // data = "";

        var mTop = $(window).height() / 2 - data.bg.height / 2;
        var mLeft = $(window).width() / 2 - data.bg.width / 2;

        var mTopOpt = $(window).height() + ($(window).height() - data.opt.height) / 2
        var mLeftOpt = $(window).width() / 2 - data.opt.width / 2;

        var mTopBoot = $(window).height() * 2 + ($(window).height() - data.boot.height) / 2
        var mLeftBoot = $(window).width() / 2 - data.boot.width / 2;

        $('.workspace').css({
            "position": "absolute",
            "top": mTop,
            "left": mLeft,
            "background-image": "url(" + (theme ? (data.bg.url + '&id=' + ID + '&token=' + TOKEN) : data.bg.url) + ")",
            "width": data.bg.width,
            "height": data.bg.height,
            "background-size": data.bg.width + "px " + data.bg.height + "px"
        });

        $('.workspaceOption').css({
            "position": "absolute",
            "top": mTopOpt,
            "left": mLeftOpt,
            "background-image": "url(" + (theme ? (data.opt.url + '&id=' + ID + '&token=' + TOKEN) : data.opt.url) + ")",
            "width": data.opt.width,
            "height": data.opt.height
        })

        $('.workspaceBoot').css({
            "position": "absolute",
            "top": mTopBoot,
            "left": mLeftBoot,
            "background-image": "url(" + (theme ? (data.boot.url + '&id=' + ID + '&token=' + TOKEN) : data.boot.url) + ")",
            "width": data.boot.width,
            "height": data.boot.height
        });
        $('.workspaceBoot .downloadingpb').css("width", data.boot.width - 80 + "px");
        $('.workspaceBoot .downloading').css('width', data.boot.width + "px");


        $('body').append('<setting name="' + data.name + '" version="' + data.setting.versionserv + '" premium="' + data.setting.premium + '" opt="' + data.setting.opt + '" forge="' + data.setting.forge + '" link_update="' + data.link + '"></setting>');

        $('.name').val(data.name);
        $('.link_update').val(data.link);
        $('.selectheaderbox').text(data.setting.versionserv);
        // data.setting.premium ? $('.premium').prop('checked', true) : $('.premium').prop('checked', false);
        // data.setting.opt ? $('.optSelect').prop('checked', true) : $('.optSelect').prop('checked', false);
        // data.setting.forge ? $('.forge').prop('checked', true) : $('.forge').prop('checked', false);

        $('.optSelect')[0].checked = data.setting.opt;
        $('.premium')[0].checked = data.setting.premium;
        $('.forge')[0].checked = data.setting.forge;


        if (data.setting.opt) {
            $('.option').show();
            $('.workspaceOption').show();
        } else {
            $('.option').hide();
            $('.workspaceOption').hide();
        }


        for (var i in data) {
            if (i == 'rect') {
                var k = 0;
                for (var j in data.rect) {
                    var rect = new RectElement(u.id(), data.rect[k].name, data.rect[k].x, data.rect[k].y, data.rect[k].width, data.rect[k].height);
                    if (data.rect[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(rect);
                    } else {
                        workspaceOption.addAndDraw(rect);
                    }
                    rect.setZ(data.rect[k].z);
                    rect.setBackground(data.rect[k].couleur);
                    rect.setColorHover(data.rect[k].couleurHover);
                    rect.setColorHoverActif(data.rect[k].couleurHoverActif);
                    rect.setArrondCorner(data.rect[k].arrondCorner);
                    rect.setOpacity(data.rect[k].opacity);
                    rect.setBordersize(data.rect[k].bordersize);
                    rect.setBordercolor(data.rect[k].bordercolor);
                    k++;
                }
            }
            if (i == 'button') {
                var k = 0;
                for (var j in data.button) {
                    var button = new ButtonElement(u.id(), data.button[k].name, data.button[k].x, data.button[k].y, data.button[k].width, data.button[k].height, (theme ? (data.button[k].url + '&id=' + ID + '&token=' + TOKEN) : data.button[k].url), (theme ? (data.button[k].url_hover + '&id=' + ID + '&token=' + TOKEN) : data.button[k].url_hover));
                    if (data.button[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(button);
                    } else {
                        workspaceOption.addAndDraw(button);
                    }
                    button.setZ(data.button[k].z);
                    button.setAction(data.button[k].action);
                    k++;
                }
            }
            if (i == 'textfield') {
                var k = 0;
                for (var j in data.textfield) {
                    var textfield = new TextfieldElement(u.id(), data.textfield[k].name, data.textfield[k].x, data.textfield[k].y, data.textfield[k].width, data.textfield[k].height);
                    if (data.textfield[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(textfield);
                    } else {
                        workspaceOption.addAndDraw(textfield);
                    }
                    textfield.setZ(data.textfield[k].z);
                    textfield.setBackground(data.textfield[k].bgcolor);
                    textfield.setColor(data.textfield[k].color);
                    textfield.setType(data.textfield[k].type);
                    textfield.setTextAlign(data.textfield[k].textalign);
                    textfield.setBordersize(data.textfield[k].bordersize);
                    textfield.setBordercolor(data.textfield[k].bordercolor);
                    textfield.setAction(data.textfield[k].action);
                    k++;
                }
            }
            if (i == 'fa') {
                var k = 0;
                for (var j in data.fa) {
                    var fonta = new FaElement(u.id(), data.fa[k].name, data.fa[k].x, data.fa[k].y, data.fa[k].iconid);
                    if (data.fa[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(fonta);
                    } else {
                        workspaceOption.addAndDraw(fonta);
                    }
                    fonta.setColor(data.fa[k].couleur);
                    fonta.setColorHover(data.fa[k].couleurHover);
                    fonta.setColorHoverActif(data.fa[k].couleurHoverActif);
                    fonta.setZ(data.fa[k].z);
                    fonta.setAction(data.fa[k].action);
                    fonta.setSize(data.fa[k].taille);
                    k++;
                }
            }
            if (i == 'image') {
                var k = 0;
                for (var j in data.image) {
                    var imgE = new ImgElement(u.id(), data.image[k].name, data.image[k].x, data.image[k].y, data.image[k].width, data.image[k].height, (theme ? (data.image[k].url + '&id=' + ID + '&token=' + TOKEN) : data.image[k].url));
                    if (data.image[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(imgE);
                    } else {
                        workspaceOption.addAndDraw(imgE);
                    }
                    imgE.setZ(data.image[k].z);
                    k++;
                }
            }
            if (i == 'font') {
                var k = 0;
                for (var j in data.font) {
                    var font = new FontElement(u.id(), data.font[k].name, data.font[k].x, data.font[k].y, data.font[k].fontfamily);
                    if (data.font[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(font);
                    } else {
                        workspaceOption.addAndDraw(font);
                    }
                    font.setColor(data.font[k].couleur);
                    font.setColorHover(data.font[k].couleurHover);
                    font.setColorHoverActif(data.font[k].couleurHoverActif);
                    font.setAction(data.font[k].action);
                    font.setText(data.font[k].text);
                    font.setSize(data.font[k].taille);
                    font.setFontFamily(data.font[k].fontfamily);
                    font.setZ(data.font[k].z);
                    k++;
                }
            }
            if (i == 'progressbar') {
                var k = 0;
                for (var j in data.progressbar) {
                    var pb = new ProgressbarElement(u.id(), data.progressbar[k].name, data.progressbar[k].x, data.progressbar[k].y, data.progressbar[k].width, data.progressbar[k].height);
                    if (data.progressbar[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(pb);
                    } else {
                        workspaceOption.addAndDraw(pb);
                    }
                    pb.setBackground(data.progressbar[k].colorfond);
                    pb.setColor(data.progressbar[k].colorbar);
                    pb.setZ(data.progressbar[k].z);
                    pb.setBordersize(data.progressbar[k].bordersize);
                    pb.setBordercolor(data.progressbar[k].bordercolor);
                    pb.setBorderradius(data.progressbar[k].arrondis);
                    k++;
                }
            }
            if (i == 'head') {
                var k = 0;
                for (var j in data.head) {
                    var head = new HeadElement(u.id(), data.head[k].name, data.head[k].x, data.head[k].y, data.head[k].width, data.head[k].height, 'DevQuentin');
                    if (data.head[k].workspace == 'WorkspacePanel') {
                        workspace.addAndDraw(head);
                    } else {
                        workspaceOption.addAndDraw(head);
                    }
                    head.setZ(data.head[k].z);
                    k++;
                }
            }
            if (i == 'importFont') {
                var k = 0;
                for (var j in data.importFont) {
                    $('head').append('<style>@font-face{' +
                        'font-family: "' + data.importFont[k].name + '";' +
                        'src: url("' + data.importFont[k].url + '");' +
                        '}</style>');
                    $('body').append('<div class="importFont" name="' + data.importFont[k].name + '" url="' + data.importFont[k].url + '"></div>');
                    k++;
                }
            }
        }

        var setting = new SettingPanel();
        setting.setBg($('.textfield2 .background'), $('.workspace').css('background-image'));
        setting.update($('.textfield2 .width'), $('.workspace').css('width'));
        setting.update($('.textfield2 .height'), $('.workspace').css('height'));
    }

});
