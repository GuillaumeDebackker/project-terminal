var Goto = Class.extend(function () {

    this.constructor = function () {
        $('body').append('<ul class="goto"></ul>')
        $('.goto').append('<li><i class="fa fa-space-shuttle launcher"></i></li>')
        $('.goto').append('<li><i class="fa fa-cog option"></i></li>')
        $('.goto').append('<li><i class="fa fa-sign-in bootstrap"></i></li>')
        $('.launcher').click(function () {

            if (STATE != 'ACHAT') {
                $('.button').show();
                $('.faIcon').show();
                $('.fonts').show();
                $('.tools').show();
                $('.arbo').show();
            }

            $('editor').attr('switcher', 'workspace');
            $('.workspace').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, 0px)"
            });
            $('.workspaceOption').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, 0px)"
            });
            $('.workspaceBoot').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, 0px)"
            });
            var setting = new SettingPanel();
            setting.setBg($('.textfield2 .background'), $('.workspace').css('background-image'));
            setting.update($('.textfield2 .width'), $('.workspace').css('width'));
            setting.update($('.textfield2 .height'), $('.workspace').css('height'));
        });
        $('.option').click(function () {

            if (STATE != 'ACHAT') {
                $('.button').show();
                $('.faIcon').show();
                $('.fonts').show();
                $('.tools').show();
            }

            $('editor').attr('switcher', 'workspaceOption');
            $('.workspace').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() + "px)"
            });
            $('.workspaceOption').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() + "px)"
            });
            $('.workspaceBoot').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() + "px)"
            });
            var setting = new SettingPanel();
            setting.setBg($('.textfield2 .background'), $('.workspaceOption').css('background-image'));
            setting.update($('.textfield2 .width'), $('.workspaceOption').css('width'));
            setting.update($('.textfield2 .height'), $('.workspaceOption').css('height'));
        });
        $('.bootstrap').click(function () {

            $('.button').hide();
            $('.faIcon').hide();
            $('.fonts').hide();
            $('.tools').hide();
            $('.arbo').hide();

            $('editor').attr('switcher', 'workspaceBoot');
            $('.workspace').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() * 2 + "px)"
            });
            $('.workspaceOption').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() * 2 + "px)"
            });
            $('.workspaceBoot').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, -" + $(window).height() * 2 + "px)"
            });
            var setting = new SettingPanel();
            setting.setBg($('.textfield2 .background'), $('.workspaceBoot').css('background-image'));
            setting.update($('.textfield2 .width'), $('.workspaceBoot').css('width'));
            setting.update($('.textfield2 .height'), $('.workspaceBoot').css('height'));
        });
    }

});
