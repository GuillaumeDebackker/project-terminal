let saveVar = true;

var Save = Class.extend(function () {

    var error = new Array();
    var premiumText = false;


    this.validate = function () {
        var notif = new Notif().setType('ERROR');
        notif.setTitle('Impossible de sauvegarder');
        $('.element').each(function () {
            if ($(this).hasClass('ButtonElement')) {
                if (typeof $(this).attr('action') == typeof undefined) {
                    notif.add('<li>Action button non-défini - ' + $(this).attr('name') + '</li>');
                }
            }
            if ($(this).hasClass('FaElement')) {
                if (typeof $(this).attr('action') == typeof undefined) {
                    notif.add('<li>Action symbole non-définie - ' + $(this).attr('name') + '</li>');
                }
            }
            if ($(this).hasClass('textfield')) {
                if (typeof $(this).attr('action') == 'undefined') {
                    notif.add('<li>Action champs textes non-définie - ' + $(this).attr('name') + '</li>');
                }
                if ($(this).attr('action') != 'password' && $('setting').attr('premium') != 'yes') {
                    notif.add('<li>Veuillez reliez un champ text à un mot de passe</li>');
                }
            }
        });

        if ($('.ProgressbarElement').length > 1) {
            notif.add('<li>Une seule bar de progression autorisée</li>');
        }
        if ($('.ProgressbarElement').length < 1) {
            notif.add('<li>Veuillez ajouter au moins une bar de progression</li>');
        }
        if (typeof $('setting').attr('version') == undefined) {
            notif.add('<li>Veuillez spécifier une version</li>');
        }
        if (typeof $('setting').attr('name') == undefined) {
            notif.add('<li>Veuillez renseigner le nom de votre serveur</li>');
        }

        if (notif.hasContent()) {
            notif.show();
            return false;
        } else {
            notif.clear();
            return true;
        }
    }

    this.save = function () {

        if (saveVar) {
            saveVar = false;

            var settingsObj = $('setting');
            var json = '{"name" : "' + settingsObj.attr('name') + '", ';
            var i = 0;

            json += '"link" : "' + settingsObj.attr('link_update') + '", '

            var bg = '"bg" : {  \
								"url" : "' + $('.workspace').css('background-image').replace('url(','').replace(')','').replace(/\"/gi, "") + '", \
								"width" : ' + parseInt($('.workspace').width()) + ', \
								"height" : ' + parseInt($('.workspace').height()) + '\
							},';

            var opt = '"opt" : {  \
								"url" : "' + $('.workspaceOption').css('background-image').replace('url(','').replace(')','').replace(/\"/gi, "") + '", \
								"width" : ' + parseInt($('.workspaceOption').width()) + ', \
								"height" : ' + parseInt($('.workspaceOption').height()) + '\
							},';

            var boot = '"boot" : {  \
								"url" : "' + $('.workspaceBoot').css('background-image').replace('url(','').replace(')','').replace(/\"/gi, "") + '", \
								"width" : ' + parseInt($('.workspaceBoot').width()) + ', \
								"height" : ' + parseInt($('.workspaceBoot').height()) + '\
							},';


            // console.log(settingsObj.attr('premium'));
            console.log("=========================================");
            console.log("premium => " + (settingsObj.attr('premium') === 'yes' || settingsObj.attr('premium') === 'true'));
            console.log("premium => " + (settingsObj.attr('premium') === "true"));
            console.log("opt => " + (settingsObj.attr('opt') === 'yes' || settingsObj.attr('opt') === true));

            var setting = '"setting" : {  \
											"nomServeur": "' + settingsObj.attr('name') + '", \
											"premium" : "' + (settingsObj.attr('premium') === 'yes' || settingsObj.attr('premium') === 'true') + '", \
											"opt" : "' + (settingsObj.attr('opt') === 'yes' || settingsObj.attr('opt') === 'true') + '", \
											"forge" : "' + (settingsObj.attr('forge') === 'yes' || settingsObj.attr('forge') === 'true') + '", \
											"versionserv" : "' + settingsObj.attr('version') + '" \
										},';

            var button = '"button" : [ ';
            var textfield = '"textfield" : [ ';
            var fa = '"fa" : [ ';
            var image = '"image" : [ ';
            var font = '"font" : [ ';
            var rect = '"rect" : [ ';
            var head = '"head" : [ ';
            var progressbar = '"progressbar" : [ ';


            var importFont = '"importFont" : [ ';

            var array = new Array();

            $('.importFont').each(function () {
                if (array.indexOf($(this).attr('name')) == -1) {
                    importFont += '	{\
                                                "name" : "' + $(this).attr('name') + '",\
                                                "url" : "' + $(this).attr('url') + '"\
                                            },';
                    array.push($(this).attr('name'));
                }
            });


            $('.element').each(function () {

                const util = new Util();

                if ($(this).hasClass('ButtonElement')) {
                    button += '{\
			                          "name" : "' + $(this).attr('name') + '", \
			                          "id" : "' + util.getIdFromElement($(this)) + '", \
			                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
			                          "action" : "' + $(this).attr('action') + '", \
			                          "url" : "' + $(this)[0].src + '", \
			                          "url_hover" : "' + $(this).attr('data-alt-src') + '", \
			                          "width" : ' + parseInt($(this).width()) + ', \
			                          "height" : ' + parseInt($(this).height()) + ', \
			                          "z" : "' + parseInt($(this).css('z-index')) + '", \
			                          "x" : ' + parseInt($(this).position().left) + ', \
			                          "y" : ' + parseInt($(this).position().top) + ' \
			                       },';
                }
                if ($(this).hasClass('TextfieldElement')) {
                    textfield += '{\
				                          "name" : "' + $(this).attr('name') + '", \
				                          "id" : "' + util.getIdFromElement($(this)) + '", \
				                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
				                          "width" : ' + parseInt($(this).width()) + ', \
				                          "height" : ' + parseInt($(this).height()) + ', \
				                          "type" : "' + $(this).attr('type') + '", \
				                          "textalign" : "' + $(this).css('text-align') + '", \
				                          "color" : "' + $(this).css('color') + '", \
				                          "bgcolor" : "' + String($(this).css('background-color')) + '", \
				                          "bordersize" : "' + $(this).css('border-width').replace('px', '') + '", \
				                          "bordercolor" : "' + $(this).css('border-color') + '", \
				                          "z" : "' + parseInt($(this).css('z-index')) + '", \
				                          "action" : "' + $(this).attr('action') + '", \
				                          "x" : ' + parseInt($(this).position().left) + ', \
				                          "y" : ' + parseInt($(this).position().top) + ' \
				                      },';
                }
                if ($(this).hasClass('FaElement')) {
                    fa += '{\
	                          "name" : "' + $(this).attr('name') + '", \
	                          "id" : "' + util.getIdFromElement($(this)) + '", \
	                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
	                          "action" : "' + $(this).attr('action') + '", \
	                          "taille" : "' + parseInt($(this).css('font-size').replace('px', '')) + '", \
	                          "couleur" : "' + $(this).css('color') + '", \
	                          "couleurHover" : "' + $(this).attr('hover') + '", \
	                          "couleurHoverActif" : "' + $(this).attr('hoverset') + '", \
	                          "width" : ' + parseInt($(this).width()) + ', \
	                          "height" : ' + parseInt($(this).height()) + ', \
	                          "z" : "' + parseInt($(this).css('z-index')) + '", \
	                          "x" : ' + parseInt($(this).position().left) + ', \
	                          "y" : ' + parseInt($(this).position().top) + ', \
	                          "iconid" : "' + parseInt($(this).attr('iconid')) + '" \
	                        },';
                }
                if ($(this).hasClass('ImgElement')) {
                    image += '{\
		                          "name" : "' + $(this).attr('name') + '", \
		                          "id" : "' + util.getIdFromElement($(this)) + '", \
		                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
		                          "url" : "' + $(this)[0].src + '", \
		                          "width" : ' + parseInt($(this).width()) + ', \
		                          "height" : ' + parseInt($(this).height()) + ', \
		                          "z" : "' + parseInt($(this).css('z-index')) + '", \
		                          "x" : ' + parseInt($(this).position().left) + ', \
		                          "y" : ' + parseInt($(this).position().top) + ' \
		                        },';
                }
                if ($(this).hasClass('FontElement')) {
                    font += '{\
	                          "name" : "' + $(this).attr('name') + '", \
	                          "id" : "' + util.getIdFromElement($(this)) + '", \
	                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
	                          "action" : "' + $(this).attr('action') + '", \
	                          "text" : "' + $(this).text() + '", \
	                          "taille" : "' + parseInt($(this).css('font-size').replace('px', '')) + '", \
	                          "couleur" : "' + $(this).css('color') + '", \
	                          "couleurHover" : "' + $(this).attr('hover') + '", \
	                          "couleurHoverActif" : "' + $(this).attr('hoverset') + '", \
	                          "fontfamily" : "' + $(this).css('font-family').replace(/"/g, '') + '", \
	                          "width" : ' + parseInt($(this).width()) + ', \
	                          "height" : ' + parseInt($(this).height()) + ', \
	                          "z" : "' + parseInt($(this).css('z-index')) + '", \
	                          "x" : ' + parseInt($(this).position().left) + ', \
	                          "y" : ' + parseInt($(this).position().top) + ' \
	                        },';
                }
                // "opacity" : ' + $(this).css('opacity') + ', \
                //"couleur" : "' + String($(this).css('background-color')).split(')')[0] + ')", \
                if ($(this).hasClass('RectElement')) {
                    rect += '{\
	                          "name" : "' + $(this).attr('name') + '", \
	                          "id" : "' + util.getIdFromElement($(this)) + '", \
	                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
	                          "arrondCorner" : "' + String($(this).css('border-radius')).replace('px', '') + '", \
	                          "couleur" : "' + String($(this).css('background-color')) + '", \
	                          "couleurHover" : "' + $(this).attr('hover') + '", \
	                          "couleurHoverActif" : "' + $(this).attr('hoverset') + '", \
	                          "bordersize" : "' + $(this).css('border-width').replace('px', '') + '", \
	                          "bordercolor" : "' + $(this).css('border-color') + '", \
	                          "width" : ' + parseInt($(this).width()) + ', \
	                          "height" : ' + parseInt($(this).height()) + ', \
	                          "z" : "' + parseInt($(this).css('z-index')) + '", \
	                          "x" : ' + parseInt($(this).position().left) + ', \
	                          "y" : ' + parseInt($(this).position().top) + ' \
	                        },';
                }
                if ($(this).hasClass('HeadElement')) {
                    head += '{\
	                          "name" : "' + $(this).attr('name') + '", \
	                          "id" : "' + util.getIdFromElement($(this)) + '", \
	                          "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
	                          "width" : ' + parseInt($(this).width()) + ', \
	                          "height" : ' + parseInt($(this).height()) + ', \
	                          "z" : "' + parseInt($(this).css('z-index')) + '", \
	                          "x" : ' + parseInt($(this).position().left) + ', \
	                          "y" : ' + parseInt($(this).position().top) + ' \
	                        },';
                }
                if ($(this).hasClass('ProgressbarElement')) {
                    let radius =  $(this).css('border-radius').replace('px', '');
                    progressbar += '{\
                                      "name" : "' + $(this).attr('name') + '", \
                                      "id" : "' + util.getIdFromElement($(this)) + '", \
                                      "workspace" : "' + ($(this).hasClass('WorkspacePanel') ? 'WorkspacePanel' : 'WorkspaceOptPanel' ) + '", \
                                      "width" : ' + parseInt($(this).width()) + ', \
                                      "height" : ' + parseInt($(this).height()) + ', \
                                      "colorbar" : "' + String($(this).children().css('background-color')) + '", \
                                      "colorfond" : "' + String($(this).css('background-color')) + '", \
                                      "bordersize" : "' + $(this).css('border-width').replace('px', '') + '", \
                                      "bordercolor" : "' + $(this).css('border-color') + '", \
                                      "z" : "' + parseInt($(this).css('z-index')) + '", \
                                      "x" : ' + parseInt($(this).position().left) + ', \
                                      "y" : ' + parseInt($(this).position().top) + ' , \
                                      "arrondis" : ' + (radius === "" ? 0 : radius) + ' \
                                    },';
                }


            });
            button = button.substring(0, button.length - 1) + '],';
            textfield = textfield.substring(0, textfield.length - 1) + '],';
            fa = fa.substring(0, fa.length - 1) + '],';
            image = image.substring(0, image.length - 1) + '],';
            font = font.substring(0, font.length - 1) + '],';
            rect = rect.substring(0, rect.length - 1) + '],';
            head = head.substring(0, head.length - 1) + '],';
            progressbar = progressbar.substring(0, progressbar.length - 1) + '],';
            importFont = importFont.substring(0, importFont.length - 1) + ']';

            json += (setting + bg + opt + boot + button + textfield + fa + image + font + rect + head + progressbar + importFont + '}');

            const successFlash = function () {
                var notif = new Notif().setType('SUCCESS');
                notif.setAutoDestroy(true);
                notif.setTitle('Launcher sauvegardé');
                notif.add('<li>Votre launcher à bien été sauvegardé.</li>');
                notif.show();
            }

            const errorFlash = function () {
                var notif = new Notif().setType('ERROR');
                notif.setAutoDestroy(true);
                notif.setTitle('Impossible de sauvegardé le launcher');
                notif.show();
            }

            SOCKET.emit('SAVE', {json: json, token: TOKEN, cookie: COOKIE});


            // $.ajax({
            // 	// url : '@Url.Action("save/' + ID + '/' + TOKEN + '", "editor")',
            //     // url : RES + 'save.php?id=' + ID + '&token=' + TOKEN,
            // 	url: '/editor/save/'+ TOKEN,
            //  type : 'POST',
            //  data : 'json=' + json,
            //  // data : '{"data":"value"}',
            //  success : function(code_html, statut){
            //      if (code_html.indexOf('SUCCESS') > -1) {
            //          successFlash();
            //      }else{
            //          errorFlash();
            //      }
            //  },
            //  error : function(resultat, statut, erreur){
            //      errorFlash();
            //  },
            //  complete : function(resultat, statut){}
            // });

            // this.ajax('', RES + 'save.php', 'json=' + json, true);
            // this.ajax('', '@Url.Action("save", "editor", new { id = Model.Id })', 'json=' + json, true);

            // $.post(BASE + 'save/' + ID + '/' + TOKEN, {jsonr: json}, function(data, textStatus, xhr) {
            // 	console.log(data);
            // });
            setTimeout(() => {
                console.log(saveVar);
                saveVar = true;
            }, 3000);
        } else {
            console.log('NOT_VALIDATED');
        }
    }

    this.ajax = function (elementID, filename, str, post) {
        var ajax;
        if (window.XMLHttpRequest) {
            ajax = new XMLHttpRequest();//IE7+, Firefox, Chrome, Opera, Safari
        } else if (ActiveXObject("Microsoft.XMLHTTP")) {
            ajax = new ActiveXObject("Microsoft.XMLHTTP");//IE6/5
        } else if (ActiveXObject("Msxml2.XMLHTTP")) {
            ajax = new ActiveXObject("Msxml2.XMLHTTP");//other
        } else {
            alert("Error: Your browser does not support AJAX.");
            return false;
        }
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                // document.getElementById(elementID).innerHTML=ajax.responseText;
                console.log(ajax.responseText);
            }
        }
        if (post == false) {
            ajax.open("GET", filename + str, true);
            ajax.send(null);
        } else {
            ajax.open("POST", filename, true);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajax.send(str);
        }
        return ajax;
    }

});
