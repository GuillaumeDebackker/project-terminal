/**
 * Created by Anonymous on 27/05/2017.
 */

let exportC = true;

class Listening {

    static registerEvents() {

        //sauvegarde
        SOCKET.on('SAVE_SUCCESS', (msg) => {
            new Notif().showNotifInfo(msg, 'SUCCESS');
        });
        SOCKET.on('SAVE_FAILED', (msg) => {
            new Notif().showNotifInfo(msg, 'ERROR');
        });

        //Redirect
        SOCKET.on('REDIRECT', (msg) => {
            window.location.replace(msg);
        });

        //Receive msg
        SOCKET.on('BROADCAST_MSG', (msg) => {
            console.log(msg);
            new Notif().showNotifInfo(msg, 'SUCCESS');
        });
        SOCKET.on('CONNECT_FAILED', (msg) => {
            console.log(msg);
            new Notif().showNotifInfo("Impossible de ce connecter, redirection...", 'ERROR');
            window.setTimeout(function(){
                window.location.href = "http://kelix.fr/client";
            }, 5000);
        });


        //compilation
        SOCKET.on('COMP_SUCCESS', (msg) => {
            new Notif().showNotifInfo(msg, 'SUCCESS');
            $('.eventComp').remove();
            $('body').append('<div class="eventComp"><div class="alertClose">×</div><div class="titleEvt">Compilation</div><div class="contentEvt">Compilation terminée</div><a href="/editor/dl/launcher/' + TOKEN + '" target="_blank" class="btnEvent" id="launcherDownload">Launcher</a><a href="/editor/dl/bootstarp/' + TOKEN + '" target="_blank" class="btnEvent" id="bootDownload">Bootstrap</a></div>');
            $('.alertClose').click(() => {
                $('.eventComp').remove();
            });
            exportC = true;
        });
        SOCKET.on('COMP_FAILED', (msg) => {
            new Notif().showNotifInfo(msg, 'ERROR');
            $('.eventComp').remove();
            $('body').append('<div class="eventComp"><div class="alertClose">×</div><div class="titleEvt">Compilation</div><div class="contentEvt">Compilation terminée avec des erreurs</div></div>');
            $('.alertClose').click(() => {
                $('.eventComp').remove();
            });
            exportC = true;
        });


        //Click export
        $('.export').click(function (event) {
            if (exportC) {
                $('.eventComp').remove();
                SOCKET.emit('COMPILE', {token: TOKEN});
                $('body').append('<div class="eventComp"><div class="titleEvt">Compilation</div><div class="contentEvt">Compilation en cours</div><div class="barEvt"></div></div>');
                exportC = false;
            }
        });


        //Keyboard event
        $(window).keydown(function (event) {
            if ((event.keyCode === 107 && event.ctrlKey) || (event.keyCode === 109 && event.ctrlKey)) {
                event.preventDefault();
            }
            $(window).bind('mousewheel DOMMouseScroll', function (event) {
                if (event.ctrlKey) {
                    event.preventDefault();
                }
            });
            if (event.ctrlKey && event.keyCode === 83) {
                new Save().save();
                event.preventDefault();
            }
        });

        $(window).bind('beforeunload', function() {
            return 'Êtes-vous sûr de vouloir vous quitter  ?';
        });

    }

}