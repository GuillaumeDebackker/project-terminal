var Notif = Class.extend(function () {


    this.type = 'INFO';
    this.title = '';
    this.num = 0;
    this.autoDestroy = false;
    this.content = '<span class="notifTitle">%TITLE%</span>\
					<ul class="notifScroll">';


    this.constructor = function () {
        $('.notifContent').empty();
    }

    this.setAutoDestroy = function (autoDestroy) {
        this.autoDestroy = autoDestroy;
        return this;
    }

    //INFO, ERROR, SUCCESS
    this.setType = function (type) {
        this.type = type;
        return this;
    }

    this.setTitle = function (title) {
        this.title = title;
    }

    this.getContent = function () {
        this.content = this.content.replace('%TITLE%', this.title);
        return this.content + '</ul>';
    }

    this.add = function (content) {
        this.content += content;
        this.num++;
    }

    this.show = function () {
        var clear = ($('.notifContainer').css('display') == 'none') ? false : true;
        if (this.content.length > 0) {
            if (clear) $('.notifContent').empty();

            $('.notifContainer .notifContent').append(this.getContent());
            $('.notifContainer').addClass(this.getClassFromType());
            if (this.num > 1) this.setScrollbar();

            if (this.autoDestroy) {
                $('.notifContainer').slideDown("slow").delay(3000).slideUp();
            } else {
                $('.notifContainer').slideDown("slow");
            }
            $('.notifClose').click(function () {
                $('.notifContainer').fadeOut();
            });
        }
    }

    this.setScrollbar = function () {
        $('.notifScroll').css('height', '60px');
        $('.notifScroll').mCustomScrollbar({
            theme: "dark-thin"
        });
    }

    this.hasContent = function () {
        return this.content != "";
    }

    this.getClassFromType = function () {
        return this.type == 'SUCCESS' ? 'n_success' : this.type == 'INFO' ? 'n_info' : 'n_error';
    }

    this.showNotifInfo = function (msg, type) {
        const notif = new Notif().setType(type ? type : 'SUCCESS');
        notif.setAutoDestroy(true);
        notif.setTitle(msg);
        notif.show();
    }

});