var Util = Class.extend(function(){

	this.sleep = function(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
				break;
			}
		}
	}

	this.readFile = function(file){
		var result = null;
		var rawFile = new XMLHttpRequest();
		rawFile.open("GET", file, false);
		rawFile.onreadystatechange = function (){
			if(rawFile.readyState === 4){
				if(rawFile.status === 200 || rawFile.status == 0){
					var allText = rawFile.responseText;
					result = allText;
				}
			}
		}
		rawFile.send(null);
		return result;
	}

	this.getIdFromElement = function(e){
		if(e == undefined) return "ID_ERROR";
		var classList = e.prop('class');
		var a = classList.match("\\bid\\S*");
		return a.toString().slice(2);
	}

	this.readJSON = function(path){
		var strJson = this.readFile(path);
		return JSON.parse(strJson);
	}

	this.id = function(){
		return this.makeid();
	}

	this.makeid = function(){
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	}

	this.selected = function(){
		$('.element').mousedown(function(){
			$('.selected').each(function() {
				$(this).removeClass('selected');
			});
			$('.focus').each(function() {
				$(this).removeClass('focus');
			});
			$('.orderboxitem_select').each(function() {
				$(this).removeClass('orderboxitem_select');
			});
			$(this).addClass('selected');
			if ($('.selected').hasClass('TextfieldElement')) {
				$(".Tdrag").remove();
				if ($('editor').attr('switcher') == 'workspaceOption') {
					$('.workspaceOption').append('<div class="Tdrag" style="z-index: 9999"><i class="fa fa-arrows Tdragz"></i></div>');
				}else{
					$('.workspace').append('<div class="Tdrag" style="z-index: 9999"><i class="fa fa-arrows Tdragz"></i></div>');
				}
				$('.Tdrag').css({
									'position' : 'absolute',
									'left' : $('.selected').position().left,
									'top' : $('.selected').position().top
								});
				$('.Tdrag').draggable();
				$(".Tdrag").bind("drag",function() {
					var offset = $(this).position();
					new OptionPanel().update($('.selected'));
					$('.selected').css({
						'left' : offset.left,
						'top' : offset.top
					});
				});
		}else {
			$(".Tdrag").remove();
		}
		$('.h' + new Util().getIdFromElement($('.selected'))).addClass('orderboxitem_select');
		});
	}



});
