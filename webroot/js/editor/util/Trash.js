var Trash = Class.extend(function () {

    this.constructor = function () {
        $('body').append('<div class="trashContainer"><i class="fa fa-trash-o trash"></i></div>')
        $('.trash').droppable({
            hoverClass: "trashhover",
            drop: function (e, ui) {
                ui.draggable.remove();
            }
        });
    }

});
