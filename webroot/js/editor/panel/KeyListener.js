var optionPanel = new OptionPanel();
var workspacePanel = new WorkspacePanel();
var util = new Util();
var ctrlDown = false;
var shiftDown = false;
var ctrlKey = 17;
var clipboardElement = false;
var selectHtml = "";
var speed = 1;

$(document).keydown(function (e) {
    var key = e.keyCode ? e.keyCode : e.which;
    var element = $('.selected');
    var position = element.position();
    var workspace = $('editor').attr('switcher');

    if ($(e.target).prop('tagName') == 'INPUT' || $(e.target).prop('tagName') == 'TEXTAREA') {
        return;
    }

    speed = shiftDown ? 2 : 1;

    switch (key) {

        case 16:
            shiftDown = true;
            break;

        case 17:
            ctrlDown = true;
            break;

        case 37: // Left Arrow
            if (workspace == 'workspace') {
                $('.WorkspacePanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.left > 0)
                        $(this).css("left", (position.left - speed) + "px");
                    }
                });
            } else if (workspace == 'workspaceOption') {
                $('.WorkspaceOptPanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.left > 0)
                        $(this).css("left", (position.left - speed) + "px");
                    }
                });
            }
            break;

        case 38: // Up arrow
            if (workspace == 'workspace') {
                $('.WorkspacePanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.top > 0)
                        $(this).css("top", (position.top - speed) + "px");
                    }
                });
            } else if (workspace == 'workspaceOption') {
                $('.WorkspaceOptPanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.top > 0)
                        $(this).css("top", (position.top - speed) + "px");
                    }
                });
            }
            break;

        case 39: // Right Arrow
            if (workspace == 'workspace') {
                $('.WorkspacePanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.left >= 0)
                        $(this).css("left", (position.left + speed) + "px");
                    }
                });
            } else if (workspace == 'workspaceOption') {
                $('.WorkspaceOptPanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.left >= 0)
                        $(this).css("left", (position.left + speed) + "px");
                    }
                });
            }
            break;

        case 46:
            $('.h' + new Util().getIdFromElement($('.selected'))).remove();
            $('.containerOpt').hide("slide", {direction: "right"}, 300);
            if (workspace == 'workspace') {
                $('.WorkspacePanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        $(this).remove();
                    }
                });
            } else if (workspace == 'workspaceOption') {
                $('.WorkspaceOptPanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        $(this).remove();
                    }
                });
            }

            break;

        case 40: // Down Arrow
            if (workspace == 'workspace') {
                $('.WorkspacePanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.top >= 0)
                        $(this).css("top", (position.top + speed) + "px");
                    }
                });
            } else if (workspace == 'workspaceOption') {
                $('.WorkspaceOptPanel').each(function () {
                    if ($(this).hasClass('selected')) {
                        var position = $(this).position();
                        //if(position.top >= 0)
                        $(this).css("top", (position.top + speed) + "px");
                    }
                });
            }
            break;

        case 65:
//    	if(ctrlDown){
//    		if($('editor').attr('switcher') == 'workspace'){
//    			$('.WorkspacePanel').each(function(){
//    				$(this).addClass('selected focus').focus();
//    			});
//    		}else if($('editor').attr('switcher') == 'workspaceOption'){
//    			$('.WorkspaceOptPanel').each(function(){
//    				$(this).addClass('selected focus').focus();
//    			});
//    		}
//    	}
            break;

        case 86:
            if (ctrlDown) {

                if (!($(e.target).hasClass('textfield')) && !($(e.target).hasClass('textfield2')) && ($(e.target).prop('tagName') != 'TEXTAREA')) {
                    createElement(element);
                }
//
//    		if (($(e.target).prop('tagName') != 'INPUT') &&  ($(e.target).prop('tagName') != 'TEXTAREA')) {
//    			createElement(element);
//    		}


//    		if ((!$(e.target).hasClass('TextfieldElement')) && ($(e.target).prop('tagName') != 'TEXTAREA')){
//    			createElement(element);
//    		}
//    		if (($(e.target).hasClass()) != 'INPUT') &&  ($(e.target).prop('tagName') != 'TEXTAREA')) {
//    		if (($(e.target).prop('tagName') != 'INPUT') &&  ($(e.target).prop('tagName') != 'TEXTAREA')) {
//    			createElement(element);
//    		}

            }
            break;
    }
    if (($(e.target).prop('tagName') != 'INPUT') && ($(e.target).prop('tagName') != 'TEXTAREA')) {
        optionPanel.update(element);
    }

}).keyup(function (e) {

    var key = e.keyCode ? e.keyCode : e.which;

    switch (key) {
        case 16:
            shiftDown = false;
            break;
        case 17:
            ctrlDown = false;
            break;
    }
});

function createElement(source) {

    if (source.hasClass('RectElement')) {
        var elem = new RectElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'));
        workspacePanel.addAndDraw(elem);
        elem.setBackground(source.css('background'));
        elem.setOpacity(source.css('opacity'));
        elem.setBordersize(source.css('border-width').replace('px', ''));
        elem.setBordercolor(source.css('border-color'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('TextfieldElement')) {
        var elem = new TextfieldElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'));
        workspacePanel.addAndDraw(elem);
        elem.setBackground(source.css('background'));
        elem.setOpacity(source.css('opacity'));
        elem.setBordersize(source.css('border-width').replace('px', ''));
        elem.setBordercolor(source.css('border-color'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('FaElement')) {
        var splititem = source.attr('class').split(' ')[3];
        var itemname = splititem.substring(4, splititem.length);
        var elem = new FaElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), itemname);
        workspacePanel.addAndDraw(elem);
        elem.setColor(source.css('color'));
        elem.setSize(source.css('font-size'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('ButtonElement')) {
        var elem = new ButtonElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'),
            source.attr('base'), source.attr('data-alt-src'));
        workspacePanel.addAndDraw(elem);
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('HeadElement')) {
        var elem = new HeadElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'),
            source.attr('head'));
        workspacePanel.addAndDraw(elem);
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('FontElement')) {
        var elem = new FontElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('font-family'));
        workspacePanel.addAndDraw(elem);
        elem.setSize(source.css('font-size'));
        elem.setColor(source.css('color'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('ImgElement')) {
        var elem = new ImgElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'), source.attr('src'));
        workspacePanel.addAndDraw(elem);
//		elem.setSize(source.css('font-size'));
//		elem.setColor(source.css('color'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }
    if (source.hasClass('ProgressbarElement')) {
        var elem = new ProgressbarElement(util.makeid(), "copyof_" + source.attr('name'),
            source.css('left'), source.css('top'), source.css('width'), source.css('height'));
        workspacePanel.addAndDraw(elem);
        elem.setBackground(source.css("background"));
        elem.setColor(source.children().css("background"));
        elem.setProgress(source.children().css("width"));
//		elem.setColor(source.css('color'));
        source.removeClass('selected');
        $(elem.getId()).addClass("selected");
        return;
    }

}
