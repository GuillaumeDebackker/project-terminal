var ItemNavbar = Element.extend(function(){

	this.iconStr = "fa-beer";
	this.showMenuitems = true;
	this.elements = new Array();

	this.constructor = function(id, name, iconStr){
		this.super(id, name);
		this.iconStr = iconStr;
	}

	this.toString = function(){
		return '<li class="' + this.name + '" id="' + this.id + '"><i class="fa ' + this.iconStr + '"></i><div class="menuitems idNAV' + this.id + '"></div></li>';
	}

	this.getId = function(){
		return 'idNAV' + this.id;
	}

	this.setShowmenuitems = function(showMenuitems){
		this.showMenuitems = showMenuitems;
		return this;
	}


	this.setAtBottom = function(){
		$('.' + this.name).css('position', 'absolute');
		$('.' + this.name).css('bottom', '0');
	}

	this.add = function(element){
		this.elements.push(element);
	}
	
	this.get = function(){
		return this.elements;
	}

	this.draw = function(){
		var result = '<ul class="scroll">';
		for (var i = 0; i < this.elements.length; i++) {
			result += this.elements[i];
		}
		result += '</ul>';
		$('.' + this.name + ' .idNAV' + this.id).append(result);
		$('.scroll').mCustomScrollbar();
	}

	this.drawPanel = function(panel){
		panel.draw();
	}

	this.addJQuery = function(){
		$('.idNAV' + this.id).css('display', 'none');

		if (this.showMenuitems) {
			var show = true;
			if (show) {
				$('.' + this.name + ', .idNAV' + this.id).click(function(){
					if ($('.idNAV' + this.id).css('display') == 'none') {
						$('.menuitems').each(function(){
							$(this).hide('fast');
						});
						$('.idNAV' + this.id).show('fast');
					}
				});
			}
//			$('.idNAV' + this.id).mouseleave(function(){
//				$('.menuitems').each(function(){
//					$(this).hide('fast');
//				});
//			});
		}
	}

});
