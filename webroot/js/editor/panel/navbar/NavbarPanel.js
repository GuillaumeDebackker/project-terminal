var NavbarPanel = Panel.extend(function () {

    this.draw = function () {
        var result = "<nav><ul>";
        for (i = 0; i < this.elements.length; i++) {
            result += this.elements[i].toString();
        }
        result += "</ul></nav>"
        $('body').append(result);
        for (i = 0; i < this.elements.length; i++) {
            this.elements[i].addJQuery();
        }
    }
});
