var WorkspaceBootPanel = Panel.extend(function () {


    this.draw = function () {
        $('body').append('<div class="' + this.name + '" style="display: block;"></div>');

        $('.' + this.name).css({
            'position': 'absolute',
            'left': this.x,
            'top': $(window).height() * 2 + ($(window).height() - this.height) / 2 + 'px',
            'width': this.width,
            'height': this.height
        });
        var result = '';

        for (i = 0; i < this.elements.length; i++) {
            result += this.elements[i].toString();
        }
        $('.' + this.name).append(result);

        for (i = 0; i < this.elements.length; i++) {
            this.elements[i].addJQuery();
        }
        this.addJQuery();
    }

    this.addJQuery = function () {
        var height = $('.workspaceBoot').height();
        var width = $('.workspaceBoot').width();
        var mTop = $(window).height() / 2 - height / 2;
        var mLeft = $(window).width() / 2 - width / 2;
        $('.workspaceBoot').css({
            "position": "absolute",
//			"top" : mTop,
            "left": mLeft,
            "width": width,
            "height": height
        });
        $(window).on('resize', function () {
            var height = $('.workspaceBoot').height();
            var width = $('.workspaceBoot').width();
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspaceBoot').css({
                "position": "absolute",
                "top": (mTop + parseInt($(window).height()) * 2) + 'px',
                "left": mLeft
//				"width" : width,
//				"height" : height
            });
            if ($('editor').attr('switcher') == 'workspaceBoot') {
                $('.workspaceBoot').css({
                    'transition': '0s all ease',
                    "-webkit-transform": "translate(0px, -" + $(window).height() * 2 + "px)"
                });
            }
        });

        var id = new Util().id();
        $('.workspaceBoot').append('<div class="downloading text' + id + '">Téléchargement en cours</div>')
        $('.text' + id).css({
            'font-family': 'Century Gothic',
            'font-size': '18px',
            'position': 'absolute',
            'left': '0px',
            'bottom': "25px",
            'width': '100%',
            'text-align': 'center',
            'color': 'white'
        });
        $('.workspaceBoot').append('<div class="downloadingpb id' + id + '" name="pg" style="width: 500px; height: 10px; background: #FFF" type="ProgressbarElement"><div class="in-bar" style="width:50%; height:100%; background: #EC8300;" ></div></div>');
        $('.id' + id).css("left", 40 + "px");
        $('.id' + id).css("bottom", "7px");
        $('.id' + id).css("width", (parseInt($('.workspaceBoot').width()) - 380) + "px");
        $('.id' + id).css("height", 15 + "px");
        $('.id' + id).css("position", "absolute");
        setInterval(function () {
            val = (parseInt($('.id' + id + ' .in-bar').width()) * 100) / parseInt($('.id' + id).width());
            val = (val >= 100 ? 0 : val + 1);
            $('.id' + id + ' .in-bar').css('width', val + "%");
        }, 100);

    }

    this.addAndDraw = function (element) {
        $('.workspaceBoot').append(element.toString());
        element.addJQuery();
        // $('.element').mousedown(function(){
        // 	$('.selected').each(function() {
        // 		$(this).removeClass('selected');
        // 	});
        // 	$(this).addClass('selected');
        // });
        //element.addClass('workspaceBoot');
    }

    this.setBackground = function (css) {
        $('.workspaceBoot').css('background', css);
    }

});
