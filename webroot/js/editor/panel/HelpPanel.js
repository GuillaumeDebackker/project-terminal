var HelpPanel = Panel.extend(function () {

    this.drawn = 'none';

    this.constructor = function () {
        this.action = new Array();
    }

    this.clear = function () {
        $('.guiDetail').remove();
        this.elements = new Array();
    }

    this.add = function (content) {
        this.elements.push(content);
    }

    this.addFunction = function (type, title, before, after) {

        element = '';

        if (typeof(before) != 'undefined') {
            this.elements.push(before);
        }

        if (type == 'TITLE') {
            element = '<h1 class="helpTitle">' + title + '</h1>';
        } else if (type == 'NAME') {
            element = '<span class="helpUnderlined">';
        } else {
            this.elements.push(type);
        }

        if (typeof(after) != 'undefined') {
            this.elements.push(after);
        }

        this.elements.push(element);
    }

    this.draw = function () {
        $('.guiContainer').append('<div class="guiDetail"></div>');

        for (i = 0; i < this.elements.length; i++) {
            $('.guiDetail').append(this.elements[i]);
        }
    }

    this.addJQuery = function () {
        $('.guiDetail').mCustomScrollbar({
            theme: "inset-2-dark"
        });

        try {
            $('#helpPicker').css('text-align', 'left');
            var colorpickerBorder = $.farbtastic("#helpPicker", {
                callback: colorpickerFuncBorder,
                width: 150,
                height: 150
            });

            function colorpickerFuncBorder(color) {
                $('.faSymbolShow').css('color', color);
                $('.faTextShow').css('color', color);
            }
        } catch (e) {
        }


        // var colorpicker = $.farbtastic("#helpPicker");
        // colorpicker.linkTo(colorpickerFunc);
        //     function colorpickerFunc(color) {
        //         $('.faSymbolShow').css('color', color);
        //         $('.faTextShow').css('color', color);
        //     }
    }


});
