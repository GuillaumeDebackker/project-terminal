var SettingPanel = Panel.extend(function () {

    this.constructor = function () {

    }

    this.constructor = function (name) {
        this.name = name;
    }

    this.addAction = function (name, desc, type, array) {

        if (type == 'TEXTFIELD') {
            this.add(desc + ' :\
					<div class="textfield2">\
				  		<input type="text" name="name" class="setting name">\
					</div>\
				  ');
        } else if (type == 'TOGGLE') {
            this.add(desc + ' :\
					<div class="switch">\
				  		<input type="checkbox" name="premium" class="setting premium">\
						<label for="toggle"><i></i></label>\
						<span></span>\
					</div>\
				  ');
        } else if (type == 'OPT') {
            this.add(desc + ' :\
					<div class="switch">\
				  		<input type="checkbox" name="opt" class="setting optSelect">\
						<label for="toggle"><i></i></label>\
						<span></span>\
					</div>\
				  ');
        } else if (type == 'SELECT') {
            let list = '';
            const tversion  = VERSIONS.split(',');
            for (let i = 0; i < tversion.length; i++){
                list += '<li>' + tversion[i] + '</li>';
            }
            this.add(desc + ' : <div class="selectbox">\
                                    <button class="selectheaderbox version" name="version">Versions</button>\
                                    <div class="selectcontent">\
                                    <ul>' + list + '</ul>\
                                  </div>\
                                </div>');
        } else if (type == 'BACKGROUND') {
            this.add(desc + ' :\
					<div class="textfield2">\
				  		<input type="text" name="name" class="setting background">\
					</div>\
				  ');
        } else if (type == 'WIDTH') {
            this.add(desc + ' :\
					<div class="textfield2">\
				  		<input type="text" name="name" class="setting width">\
					</div>\
				  ');
        } else if (type == 'HEIGHT') {
            this.add(desc + ' :\
					<div class="textfield2">\
				  		<input type="text" name="name" class="setting height">\
					</div>\
				  ');
        } else if (type === 'FORGE') {
            this.add(desc + ' :\
					<div class="switch">\
				  		<input type="checkbox" name="forge" class="setting forge">\
						<label for="toggle"><i></i></label>\
						<span></span>\
					</div>\
				  ');
        } else if (type === 'LINK_UPDATE') {
            this.add(desc + ' :\
					<div class="textfield2">\
				  		<input type="text" name="name" class="setting link_update">\
					</div>\
				  ');
        }

    }

    this.setBg = function (elem, val) {
        var pattern = '"' + '(.*)' + '"';
        var bg = val.match(pattern)[0];
        elem.val(replaceAll(bg, '"', ""));
    }

    this.update = function (elem, val) {
        elem.val(val.replace('px', ''));
    }

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    this.addJQuery = function () {
        $('.name').change(function () {
            $('setting').attr($(this).attr('name'), $(this).val());
        });
        $('.premium').change(function () {
            var premium = $(this).prop('checked') ? "yes" : "no";
            $('setting').attr($(this).attr('name'), premium);
        });
        $('.forge').change(function () {
            let forge = $(this).prop('checked') ? "yes" : "no";
            $('setting').attr($(this).attr('name'), forge);
        });
        $('.optSelect').change(function () {
            var opt = $(this).prop('checked') ? "yes" : "no";
            $('setting').attr($(this).attr('name'), opt);

            if ($(this).prop('checked')) {
                $('.option').show();
                $('.workspaceOption').show();
            } else {
                $('.option').hide();
                $('.workspaceOption').hide();
            }
        });

        $('.selectcontent ul li').click(function () {
            $('.selectheaderbox').text($(this).text());
            $('setting').attr('version', $(this).text());
        });
        $('.link_update').keyup(function () {
            $('setting').attr('link_update', $(this).val());
        });
        $('.setting .background').keyup(function () {
            if ($('editor').attr('switcher') == 'workspaceOption') {
                $('.workspaceOption').css('background-image', 'url("' + $(this).val() + '")');
            } else if ($('editor').attr('switcher') == 'workspace') {
                $('.workspace').css('background-image', 'url("' + $(this).val() + '")');
            } else if ($('editor').attr('switcher') == 'workspaceBoot') {
                $('.workspaceBoot').css('background-image', 'url("' + $(this).val() + '")');
            }
        });
        $('.setting .width').keyup(function () {
            var width = parseInt($(this).val());
            if (width < 100 || width > 1800) {
                return false;
            }
            if ($('editor').attr('switcher') == 'workspaceOption') {
                $('.workspaceOption').css('width', width + "px");
            } else if ($('editor').attr('switcher') == 'workspace') {
                $('.workspace').css('width', width + "px");
            } else if ($('editor').attr('switcher') == 'workspaceBoot') {
                $('.workspaceBoot .downloadingpb').css("width", width - 80 + "px");
                $('.workspaceBoot .downloading').css('width', width + "px");
                $('.workspaceBoot').css('width', width + "px");
            }
        });
        $('.setting .height').keyup(function () {
            var height = parseInt($(this).val());
            if (height < 100 || height > 1000) {
                return false;
            }
            if ($('editor').attr('switcher') == 'workspaceOption') {
                $('.workspaceOption').css('height', height + "px");
                $('.workspaceOption').css('top', $(window).height() + ($(window).height() - height) / 2 + $(window).height() + 'px');
            } else if ($('editor').attr('switcher') == 'workspace') {
                $('.workspace').css('height', height + "px");
                $('.workspace').css('top', ($(window).height() - height) / 2 + 'px');
            } else if ($('editor').attr('switcher') == 'workspaceBoot') {
                $('.workspaceBoot').css('height', height + "px");
                $('.workspaceBoot').css('top', ($(window).height() - height) / 2 + $(window).height() * 2 + 'px');
            }
        });
    }

    this.draw = function () {
        var result = '<ul class="scrollSetting">';
        for (i = 0; i < this.elements.length; i++) {
            result += '<li class="liSetting">' + this.elements[i] + '</li>';
        }

        //console.log(this.name);
        $('.' + this.name).append(result + '</ul>');
        $('.' + this.name).css('width', '300px');
        $('.' + this.name).mCustomScrollbar();


    }

});
