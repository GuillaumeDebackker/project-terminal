var Panel = Class.extend(function () {

    this.elements = new Array();
    this.name = '';
    this.x = 0;
    this.y = 0;
    this.width = 50;
    this.height = 50;

    this.constructor = function (name) {
        this.name = name;
    }

    this.constructor = function (name, x, y, w, h) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    this.getElements = function () {
        return this.elements;
    }

    this.add = function (element) {
        this.elements.push(element);
    }

    this.clearElements = function () {
        this.elements = [];
    }

    this.draw = function () {
    }

});