var OrderPanel = Panel.extend(function () {

    var ws = new Array();
    var opt = new Array();

    this.draw = function () {
        $('body').append('<div class="orderbox"><h4 class="arboTitle" style="text-align: center; color: white;">Arborescence des éléments</h4></div>');
        $('.orderbox').css('display', 'none');
        $('.orderbox').append('<ul ></ul>');
        $('.orderbox').mCustomScrollbar();
    }

    this.addJQuery = function () {
        var v = false;

        $('.arbo').mousedown(function () {
            if (!v) {
                $('.orderbox').show("slide", {direction: "left"}, 300);
                v = true;

                if ($('editor').attr('switcher') == 'workspaceOption') {
                    drawContent('OPT');
                } else {
                    drawContent('WS');
                }

            } else {
                $('.orderbox').hide("slide", {direction: "left"}, 300);
                v = false;
            }
        });
    }

    this.addDefault = function (element) {
        ws.push(element);
        drawContent('WS');
    }

    this.addOpt = function (element) {
        opt.push(element);
        drawContent('OPT');
    }

    function drawContent(select) {
        $('.orderboxitem').remove();
        if (select == 'OPT') {
            for (var i = 0; i < opt.length; i++) {
                $('.orderbox ul').append('<li class="orderboxitem h' + opt[i].getId() + '" name="' + opt[i].getId() + '">' + opt[i].getName() + '</li>');
                $('.orderboxitem').mousedown(function () {
                    $('.orderboxitem').each(function () {
                        $(this).removeClass('orderboxitem_select');
                    });
                    $(this).addClass('orderboxitem_select');
                    $('.selected').each(function () {
                        $(this).removeClass('selected focus');
                    });
                    $('.id' + $(this).attr('name')).addClass('selected focus');
                });
            }
        } else if (select == 'WS') {
            for (var i = 0; i < ws.length; i++) {
                $('.orderbox ul').append('<li class="orderboxitem h' + ws[i].getId() + '" name="' + ws[i].getId() + '">' + ws[i].getName() + '</li>');
                $('.orderboxitem').mousedown(function () {
                    $('.orderboxitem').each(function () {
                        $(this).removeClass('orderboxitem_select');
                    });
                    $(this).addClass('orderboxitem_select');
                    $('.selected').each(function () {
                        $(this).removeClass('selected focus orderboxitem_select');
                    });
                    $('.id' + $(this).attr('name')).addClass('selected focus');
                });
            }
        }

    }

});
var orderp = new OrderPanel();
