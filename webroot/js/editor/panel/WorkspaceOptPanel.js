var WorkspaceOptPanel = Panel.extend(function () {


    this.draw = function () {
        $('body').append('<div class="' + this.name + '" style="display: block;"></div>');

        $('.' + this.name).css({
            'position': 'absolute',
            'left': this.x,
            'top': $(window).height() + ($(window).height() - this.height) / 2 + 'px',
            'width': this.width,
            'height': this.height
        });
        var result = '';

        for (i = 0; i < this.elements.length; i++) {
            result += this.elements[i].toString();
        }
        $('.' + this.name).append(result);

        for (i = 0; i < this.elements.length; i++) {
            this.elements[i].addJQuery();
        }
        this.addJQuery();
    }

    this.addJQuery = function () {
        var height = $('.workspaceOption').height()
        var width = $('.workspaceOption').width()
        var mTop = $(window).height() / 2 - height / 2;
        var mLeft = $(window).width() / 2 - width / 2;
        $('.workspaceOption').css({
            "position": "absolute",
//			"top" : mTop,
            "left": mLeft,
            "width": width,
            "height": height
        });
        $(window).on('resize', function () {
            var height = $('.workspaceOption').height()
            var width = $('.workspaceOption').width()
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspaceOption').css({
                "position": "absolute",
                "top": $(window).height() + ($(window).height() - $('.workspaceOption').height()) / 2 + 'px',
                "left": mLeft
//				"width" : width,
//				"height" : height
            });
            if ($('editor').attr('switcher') == 'workspaceOption') {
                $('.workspaceOption').css({
                    'transition': '0s all ease',
                    "-webkit-transform": "translate(0px, -" + $(window).height() + "px)"
                });
            }
        });
    }

    this.addAndDraw = function (element) {
        $('.workspaceOption').append(element.toString());
        element.addJQuery();
        // $('.element').mousedown(function(){
        // 	$('.selected').each(function() {
        // 		$(this).removeClass('selected');
        // 	});
        // 	$(this).addClass('selected');
        // });
//		element.addClass('workspaceOption');
        $('.id' + element.getId()).addClass('WorkspaceOptPanel');
        orderp.addOpt(element);
    }

    this.setBackground = function (css) {
        $('.workspaceOption').css('background', css);
    }

});
