var OptionPanel = Panel.extend(function () {

    this.drawn = 'none';

    this.constructor = function () {
        this.action = new Array();
    }

    this.clear = function () {
        this.drawn = $('.containerOpt').css('display');
        $('.containerOpt').remove();
        this.elements = new Array();
        this.action = new Array();
    }

    this.addFunction = function (type, before, after) {

        element = '';

        if (typeof(before) != 'undefined') {
            this.elements.push(before);
        }

        if (type == 'X') {
            element = 'X = <input class="textfield" type="text" name="x" id="x"/><br>';
        } else if (type == 'Y') {
            element = 'Y = <input class="textfield" type="text" name="y" id="y"/><br>';
        } else if (type == 'WIDTH') {
            element = 'Longueur = <input class="textfield" type="text" name="width" id="width"/><br>';
        } else if (type == 'HEIGHT') {
            element = 'Largeur = <input class="textfield" type="text" name="height" id="height"/><br>';
        } else if (type == 'COORD') {
            this.elements.push('X<br> <input class="textfield" type="text" name="x" id="x"/><br>');
            this.elements.push('Y<br> <input class="textfield" type="text" name="y" id="y"/><br>');
            this.elements.push('Longueur<br> <input class="textfield" type="text" name="width" id="width"/><br>');
            element = 'Largeur<br> <input class="textfield" type="text" name="height" id="height"/><br>';
        } else if (type == 'COORDWT') {
            this.elements.push('X<br> <input class="textfield" type="text" name="x" id="x"/><br>');
            this.elements.push('Y<br> <input class="textfield" type="text" name="y" id="y"/><br>');
            this.elements.push('Longueur<br> <input class="textfield" type="text" name="width" id="width"/><br>');
            this.elements.push('Largeur<br> <input class="textfield" type="text" name="height" id="height"/><br>');
            element = 'Taille<br> <input class="textfield" type="text" name="taille" id="taille"/><br>';
        } else if (type == 'NAME') {
            element = '<h3>Nom</h3><br><input class="textfield" type="text" name="name" id="name"/><br>';
        } else if (type == 'Z') {
            element = '<h3>Superposition</h3><br><input type="button" value="-" id="minus"><input class="textfield" type="text" name="z" id="z" style="width : 202px"/><input type="button" value="+" id="plus"><br>';
        } else if (type == 'COLOR') {
            element = '<h3>Couleur</h3><br><div id="colorP"></div><br>';
        } else if (type == 'BACKGROUND') {
            element = '<h3>Background</h3><br><div id="backgroundP"></div><br>';
        } else if (type == "ALPHA") {
            element = '<h3>Opacité</h3><br><input type="range" min="0" max="100" value="100" id="opacity"/><br>';
        } else if (type == "ALPHAFA") {
            element = '<h3>Opacité</h3><br><input type="range" min="0" max="100" value="100" id="opacityfa"/><br>';
        } else if (type == 'TEXT') {
            element = '<h3>Texte</h3><br><textarea class="textfield" id="text"></textarea><br>';
        } else if (type == 'BASE') {
            this.elements.push('<h3>Image</h3><br>Image de base<br/><input class="textfield" type="text" name="baseImg" id="baseImg"/><br>');
            element = 'Image hover<br/><input class="textfield" type="text" name="hoverImg" id="hoverImg"/><br>';
        } else if (type == 'SRC') {
            this.elements.push('<h3>Image</h3><br>Image<br/><input class="textfield" type="text" name="baseImg" id="baseImg"/><br>');
        } else if (type == 'ARROND') {
            element = '<h3>Coin arrondi</h3><br><input class="textfield" type="text" name="arrond" id="arrond"/><br>';
        } else if (type == 'PGBPERCENT') {
            this.elements.push('<h3>Détails</h3><br>Pourcentage<br><input type="range" min="0" max="100" value="0" id="pgbpercent"/><br>');
        } else if (type == 'BORDER') {
            element = '<h3>Bordure</h3><br><input type="button" value="-" id="minusborder"><input class="textfield" type="text" name="border" id="border" style="width : 202px"/><input type="button" value="+" id="plusborder"><br><br>Couleur de la bordure<br><div id="colorPborder"></div><br>Opacité de la bordure<br><input type="range" min="0" max="100" value="100" id="opacityborder"/><br>';
        } else if (type == 'CONVERT') {
            element = '<h3>Convertir en bouton</h3><br><input class="buttonC" type="button" value="Convertir" id="convert">';
        } else if (type == 'FONTSELECT') {
            element = '<h3>Police</h3><br><div class="selectbox">\
														<button class="selectheaderbox" id="fontSeclect" name="fontSeclect" style="width: 276px">Police</button>\
															<div class="selectcontent fontSeclectContent">\
															<ul id="fontUL">';
            data = new Util().readJSON(RES + 'media.json');
            for (var i in data.fonts) {
                element += '<li style="font-family: ' + data.fonts[i].name + '">Kelix</li>';
            }
            element += '</ul></div></div>';
        } else {
            this.elements.push(type);
        }

        if (typeof(after) !== 'undefined') {
            this.elements.push(after);
        }

        this.elements.push(element);
    }

    this.addAction = function (action, desc) {
        var result = '';
        this.action = action;
        for (var i = 0; i < action.length; i++) {
            result += '	<div class="squaredThree">\
							<input type="checkbox" class="chk ' + (action[i] === 'hover' ? '' : 'action') + '" value="None" id="' + action[i] + '" name="' + action[i] + '" />\
							<label for="' + action[i] + '"></label>\
							' + desc[i] + '\
						</div>';
            if (action[i] === 'link') {
                result += '<div class="linkO" style="display: none"><input class="textfield" type="text" name="link" id="linkToOpen"/></div>';
            }
            if (action[i] === 'hover') {
                result += '<div class="hoverO" style="display: none"><div id="hoverPicker"></div></div>';
            }
        }
        this.addFunction(result, '<h3>Action</h3><br>');
    }

    this.addJQueryFont = function () {
        $('.fontSeclectContent ul li').click(function () {
            $('#fontSeclect').text($(this).css('font-family'));
            $('.selected').css('font-family', $(this).css('font-family'));
        });
        $('#fontUL').mCustomScrollbar();
    }

    this.addActionJQuery = function () {
        $('#center').removeClass('action');
        $('#hide').removeClass('action');
        $('.action').change(function () {
            $('.action').each(function () {
                $(this).prop("checked", false);
            });
            $(this).prop("checked", true);
            if ($('#link').prop("checked")) {
                $('.linkO').slideDown();
            } else {
                $('.linkO').slideUp();
            }
            $('.selected').attr('action', $(this).attr('name'));
        });
        $('#linkToOpen').change(function () {
            $('.selected').attr('action', 'link:' + $('#linkToOpen').val());
        });
        $('#hover').change(function () {
            if ($('#hover').prop('checked')) {
                $('.hoverO').slideDown();
                $('.selected').attr('hoverset', '1');
            } else {
                $('.hoverO').slideUp();
                $('.selected').attr('hoverset', '0');
            }
        });
        try {
            $.farbtastic("#hoverPicker", {callback: colorpickerFuncHover, width: 250, height: 250});
            function colorpickerFuncHover(color) {
                if (color === '#808080') {
                    return;
                }
                $('.selected').attr('hover', color);
            }
        } catch (e) {
        }
        $('#convert').avgrund({
            width: 500, // max is 640px
            height: 300, // max is 350px
            showClose: false, // switch to 'true' for enabling close button
            showCloseText: '', // type your text for close button
            closeByEscape: true, // enables closing popup by 'Esc'..
            closeByDocument: true, // ..and by clicking document itself
            holderClass: 'content', // lets you name custom class for popin holder..
            overlayClass: '', // ..and overlay block
            enableStackAnimation: false, // enables different type of popin's animation
            onBlurContainer: '.workspace', // enables blur filter for specified block
            openOnEvent: true, // set to 'false' to init on load
            setEvent: 'click', // use your event like 'mouseover', 'touchmove', etc.
            template: '<div class="uploadBox"><div class="uploadBoxTitle">Faites glissez l\'image ici</div><i class="fa fa-upload faupload" aria-hidden="true"></i></div>',
            onLoad: function (elem) {
                $('nav').hide("slide", {direction: "left"}, 300);
                $('.containerOpt').hide("slide", {direction: "right"}, 300);
            },
            onDisplay: function (elem) {
                $('.uploadBox').parent().css('background', 'rgba(0,0,0,0.5)');
                $(document).on('drop', '.content', function (e) {
                    if (e.originalEvent.dataTransfer) {
                        if (e.originalEvent.dataTransfer.files.length) {
                            e.preventDefault();
                            e.stopPropagation();
                            upload(e.originalEvent.dataTransfer.files);
                        }
                    }
                    return false;
                });
                var upload = function (files) {
                    var f = files[0];
                    if (f.type.match('image/jpeg')) {
                        format = 'jpg';
                    } else if (f.type.match('image/png')) {
                        format = 'png';
                    } else {
                        var notif = new Notif();
                        notif.add('<li>Le fichier doit avoir l\'extention .png ou .jpeg</li>');
                        notif.show();
                        return false;
                    }

                    var reader = new FileReader();
                    reader.onload = handleReaderLoad;
                    reader.readAsDataURL(f);
                }


                var handleReaderLoad = function (evt) {
                    $.ajax({
                        type: 'POST',
                        url: '/editor/upload/' + TOKEN,
                        data: {data: evt.target.result},
                        success: function (data, statut) {
                            // console.log(data);
                            var x = $('.selected').position().left;
                            var y = $('.selected').position().top;
                            var src = $('.selected').attr('src');
                            $('.selected').remove();
                            setTimeout(function () {
                                $('.avgrund-popin').remove();
                            }, 500);
                            $('body').removeClass('avgrund-active');
                            $('nav').show("slide", {direction: "left"}, 300);
                            $('.containerOpt').show("slide", {direction: "right"}, 300);
                            ws = new WorkspacePanel();
                            opt = new WorkspaceOptPanel();
                            button = new ButtonElement(new Util().makeid(), 'button', x, y, 'auto', 'auto', src, "/editor/access/" + TOKEN + '/' + data);
                            if ($('editor').attr('switcher') === 'workspaceOption') {
                                opt.addAndDraw(button);
                            } else {
                                ws.addAndDraw(button);
                            }
                            new Util().selected();
                        },
                        complete: function (data, statut) {
                        }
                    });
                }
            },
            onUnload: function (elem) {
                $('nav').show("slide", {direction: "left"}, 300);
                $('.containerOpt').show("slide", {direction: "right"}, 300);
            }
        });
    }

    this.addJQueryColor = function (css) {
        if (typeof css === 'undefined') {
            css = 'color';
        }
        $.farbtastic("#colorP", {callback: colorpickerFuncCOLOR, width: 250, height: 250});

        function colorpickerFuncCOLOR(color) {
            if (color === '#808080') {
                return;
            }
            $('.selected').css(css, color);
        }
    }

    this.addJQueryProgress = function () {
        $.farbtastic("#colorP", {callback: colorpickerFuncProgress, width: 250, height: 250});

        function colorpickerFuncProgress(color) {
            if (color === '#808080') {
                return;
            }
            $('.selected .in-bar').css('background', color);
        }

    }

    this.addJQueryBackground = function (css) {
        if (typeof css === 'undefined') {
            css = 'background';
        }
        $.farbtastic("#backgroundP", {
            callback: colorpickerFuncBackground,
            width: 250,
            height: 250
        });

        function colorpickerFuncBackground(color) {
            if (color === '#808080') {
                return;
            }
            $('.selected').css(css, color);
        }
    }

    this.addJQueryBorderColor = function (css) {
        if (typeof css === 'undefined') {
            css = 'border-color';
        }
        $.farbtastic("#colorPborder", {
            callback: colorpickerFuncBorder,
            width: 250,
            height: 250
        });

        function colorpickerFuncBorder(color) {
            if (color === '#808080') {
                return;
            }
            $('.selected').css(css, color);
        }
    }

    this.addJQueryTextfield = function () {
        $('#hide').change(function () {
            if ($('#hide').prop('checked')) {
                $('.selected').attr('type', 'password');
            } else {
                $('.selected').attr('type', 'text');
            }
        });
        $('#center').change(function () {
            if ($('#center').prop('checked')) {
                $('.selected').css('text-align', 'center');
            } else {
                $('.selected').css('text-align', 'left');
            }
        });
    }

    const hexDigits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];

    //Function to convert hex format to a rgb color
    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    function hex(x) {
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function rgba2hex(rgb) {
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? "#" +
            ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    }

    function convertHex(hex, opacity) {
        hex = hex.replace('#', '');
        r = parseInt(hex.substring(0, 2), 16);
        g = parseInt(hex.substring(2, 4), 16);
        b = parseInt(hex.substring(4, 6), 16);

        result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
        return result;
    }

    this.init = function (element) {
        $('#name').val(element.attr('name'));
        $('#x').val(element.position().left);
        $('#y').val(element.position().top);
        $('#width').val(element.width());
        $('#height').val(element.height());
        $('#z').val(element.css('z-index'));
        $('#taille').val(element.css('font-size').replace('px', ''));
        $('#opacityfa').val(parseFloat(element.css('opacity')) * 100);
        $('#text').val(element.text());
        $('#border').val(element.css('border-width').replace('px', ''));
        $('#fontSeclect').text(element.css('font-family'));
        if (element.hasClass('ImgElement')) {
            $('#baseImg').val(element.attr('src'));
        } else {
            if (element.prop('hover')) {
                $('#baseImg').val(element.attr('src'));
                $('#hoverImg').val(element.attr('data-alt-src'));
            } else {
                $('#baseImg').val(element.attr('base'));
                $('#hoverImg').val(element.attr('src'));
            }
        }


        $('#arrond').val(element.css('border-radius').replace('px', ''));
        if (typeof element.attr('action') != 'undefined') {
            var action = element.attr('action');
            if (action.indexOf('link') > -1) {
                $('#link').prop("checked", true);
                $('.linkO').slideDown();
                $('#linkToOpen').val(action.replace('link:', ''));
            } else {
                $('#' + action).prop("checked", true);
            }
        }
        if (element.attr('hoverset') == "0") {
            $('#hover').prop("checked", false);
            $('.hoverO').slideUp();
        } else {
            $('#hover').prop("checked", true);
            $('.hoverO').slideDown();
        }
        if (element.css('text-align') == 'center') {
            $('#center').prop("checked", true);
        }
        if (element.attr('type') == 'password') {
            $('#hide').prop("checked", true);
        }
        var width = ( 100 * parseFloat(element.children().css('width')) / parseFloat(element.css('width')) );
        $('#pgbpercent').val(width);

    }

    this.update = function (element) {
        try {
            $('#name').val(element.attr('name'));
            $('#x').val(element.position().left);
            $('#y').val(element.position().top);
            $('#width').val(element.width());
            $('#height').val(element.height());
            $('#z').val(element.css('z-index'));
            $('#taille').val(element.css('font-size').replace('px', ''));
            $('#opacity').val(parseInt(element.css('opacity')) * 100);

            $('#fontSeclect').text(element.css('font-family'));
            //$('#opacityborder').val(parseInt(element.css('opacity'))*100);

            $('#text').val(element.text());
            $('#baseImg').val(element.attr('base'));
            $('#hoverImg').val(element.attr('data-alt-src'));
            $('#arrond').val(element.css('border-radius').replace('px', ''));
            $('#baseImg').val(element.attr('type') == 'ImgElement' ? element.attr('src') : element.attr('base'));
        } catch (e) {
        }

    }

    this.change = function () {
        $('#width').keyup(function () {
            $('.selected').width($('#width').val() + 'px');
        });
        $('#height').keyup(function () {
            $('.selected').height($('#height').val() + 'px');
        });
        $('#name').keyup(function () {
            if ($('.selected').attr('type') == 'HeadElement') {
                $('.selected').attr('src', 'https://minotar.net/avatar/' + $('#name').val() + '/200');
            }
            $('.selected').attr('name', $('#name').val());
            $('.h' + new Util().getIdFromElement($('.selected'))).text($('#name').val());
        });
        $('#x').keyup(function () {
            $('.selected').css('left', $('#x').val() + 'px');
        });
        $('#y').keyup(function () {
            $('.selected').css('top', $('#y').val() + 'px');
        });
        $('#z').keyup(function () {
            $('.selected').css('z-index', $('#z').val());
        });
        $('#plus').click(function () {
            $('.selected').css('z-index', parseInt($('.selected').css('z-index')) + 1);
            $('#z').val($('.selected').css('z-index'));
        });
        $('#minus').click(function () {
            $('.selected').css('z-index', parseInt($('.selected').css('z-index')) - 1);
            $('#z').val($('.selected').css('z-index'));
        });
        $('#plusborder').click(function () {
            $('.selected').css('border-width', parseInt($('.selected').css('border-width').replace('px', '')) + 1);
            $('#border').val($('.selected').css('border-width'));
        });
        $('#minusborder').click(function () {
            $('.selected').css('border-width', parseInt($('.selected').css('border-width').replace('px', '')) - 1);
            $('#border').val($('.selected').css('border-width'));
        });
        $('#taille').keyup(function () {
            $('.selected').css('font-size', parseInt($('#taille').val()) + 'px');
        });
        $('#opacity').mousemove(function () {
            var opacity = parseFloat(($(this).val()));
            var color = rgba2hex($('.selected').css('background-color'));
            var newColor = convertHex(color, opacity);
            $('.selected').css('background-color', newColor);
        });

        $('#opacityfa').mousemove(function () {
            $('.selected').css('opacity', $(this).val() / 100);
            /*var opacity = parseFloat(($(this).val()));
             var color = rgba2hex($('.selected').css('background-color'));
             var newColor = convertHex(color, opacity);
             $('.selected').css('background-color', newColor);*/
        });

        $('#opacityborder').mousemove(function () {
            var opacityBorder = parseFloat(($('#opacityborder').val()));
            var color = rgba2hex($('.selected').css('border-color'));
            var newBorderColor = convertHex(color, opacityBorder);
            $('.selected').css('border-color', newBorderColor);
//			console.log(newBorderColor);
        });
//			$('.selected').css('opacity', parseInt($('#opacity').val())/100);
//		});

        $('#pgbpercent').mousemove(function () {
            $('.selected .in-bar').css('width', $('#pgbpercent').val() + "%");
        });
        $('#text').keyup(function () {
            $('.selected').text($('#text').val());
        });
        $('#baseImg').keyup(function () {
            $('.selected').attr('base', $('#baseImg').val());
            $('.selected').attr('src', $('#baseImg').val());
        });
        $('#hoverImg').keyup(function () {
            $('.selected').attr('data-alt-src', $('#hoverImg').val());
        });
        $('#arrond').keyup(function () {
            if ($('.selected').hasClass('ProgressbarElement')) {
                $('.selected').css('border-radius', $('#arrond').val() + 'px');
                $('.selected').children().css('border-radius', $('#arrond').val() + 'px');
            } else {
                $('.selected').css('border-radius', $('#arrond').val() + 'px');
            }
        });
        $('#x').on('mousewheel', function(event) {
            if ($(this).is(":focus")) {
                let val = parseInt($(this).val()) + event.deltaY;
                $('.selected').css('left', val + 'px');
                $(this).val(val);
            }
        });
        $('#y').on('mousewheel', function(event) {
            if ($(this).is(":focus")) {
                let val = parseInt($(this).val()) + event.deltaY;
                $('.selected').css('top', val + 'px');
                $(this).val(val);
            }
        });
        $('#width').on('mousewheel', function(event) {
            if ($(this).is(":focus")) {
                let val = parseInt($(this).val()) + event.deltaY;
                $('.selected').width(val + 'px');
                $(this).val(val);
            }
        });
        $('#height').on('mousewheel', function(event) {
            if ($(this).is(":focus")) {
                let val = parseInt($(this).val()) + event.deltaY;
                $('.selected').height(val + 'px');
                $(this).val(val);
            }
        });
        $('#taille').on('mousewheel', function(event) {
            if ($(this).is(":focus")) {
                let val = parseInt($(this).val()) + event.deltaY;
                $('.selected').css('font-size', val + 'px');
                $(this).val(val);
            }
        });
    }

    this.draw = function () {
        if ($('#BLOCKOPT').is(":checked")) {
            return;
        }

        $('body').append('<div class="containerOpt"><opt><div id="opt"><h1>OPTIONS</h1></div></opt></div>');

        if (this.drawn !== 'block') {
            $('.containerOpt').css('display', 'none');
        }

        $('.containerOpt').show("slide", {direction: "right"}, 300);


        for (i = 0; i < this.elements.length; i++) {
            $('#opt').append(this.elements[i]);
        }
        this.addJQuery();
    }

    this.addJQuery = function () {
        this.change();
        $(document).click(function (e) {
            //console.log($(e.target).parents());
//			console.log($(this).attr('id'));
//			console.log($(this).attr('class'));
//			$(e.target).parents().each(function(){
//				console.log($(this).attr('id'));
//			});
        });
        $('.containerOpt').mCustomScrollbar({
            mouseWheel: {disableOver: ["input"]},
            advanced: {autoScrollOnFocus: false}
        });
    }


});
