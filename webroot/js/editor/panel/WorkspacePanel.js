var WorkspacePanel = Panel.extend(function () {

    this.draw = function () {
        $('body').append('<div class="' + this.name + '"></div>');
        $('.' + this.name).css({
            'position': 'absolute',
            'left': this.x,
            'top': this.y,
            'width': this.width,
            'height': this.height
        });
        var result = '';

        for (i = 0; i < this.elements.length; i++) {
            result += this.elements[i].toString();
        }
        $('.' + this.name).append(result);

        for (i = 0; i < this.elements.length; i++) {
            this.elements[i].addJQuery();
        }
        this.addJQuery();
    }

    this.addJQuery = function () {
        var height = $('.workspace').height()
        var width = $('.workspace').width()
        var mTop = $(window).height() / 2 - height / 2;
        var mLeft = $(window).width() / 2 - width / 2;
        $('.workspace').css({
            "position": "absolute",
            "top": mTop,
            "left": mLeft,
            "width": width,
            "height": height
        });
        $(window).on('resize', function () {
            var height = $('.workspace').height()
            var width = $('.workspace').width()
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspace').css({
                "position": "absolute",
                "top": mTop,
                "left": mLeft,
                "width": width,
                "height": height
            });
        });
    }

    this.addAndDraw = function (element) {
        $('.workspace').append(element.toString());
        if (element.getType() == "text") {
            $('.id' + element.getId()).val("");
        }
        element.addJQuery();
        // $('.element').mousedown(function(){
        // 	$('.selected').each(function() {
        // 		$(this).removeClass('selected');
        // 	});
        // 	$(this).addClass('selected');
        // });

        $('.id' + element.getId()).addClass('WorkspacePanel');

//		element.addClass('workspace');
        orderp.addDefault(element);
    }

    this.setBackground = function (css) {
        $('.workspace').css('background', css);
    }

});
