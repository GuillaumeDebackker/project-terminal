var UploadPanel = Panel.extend(function () {

    var opt = new WorkspaceOptPanel();
    var workspace = new WorkspacePanel();
    var format = '';
    var filename = '';


    this.constructor = function () {
        $(document).on('dragenter', 'html', function () {
            // $(this).css('border', '3px dashed red');
            return false;
        });
        $(document).on('dragover', 'html', function (e) {
            e.preventDefault();
            e.stopPropagation();
            // $(this).css('border', '3px dashed red');
            return false;
        });
        $(document).on('dragleave', 'html', function (e) {
            e.preventDefault();
            e.stopPropagation();
            // $(this).css('border', '3px dashed #BBBBBB');
            return false;
        });
        $(document).on('drop', 'html', function (e) {
            if (e.originalEvent.dataTransfer) {
                if (e.originalEvent.dataTransfer.files.length) {
                    e.preventDefault();
                    e.stopPropagation();
                    // $(this).css('border', '3px dashed green');
                    upload(e.originalEvent.dataTransfer.files);
                }
            } else {
                // $(this).css('border', '3px dashed #BBBBBB');
            }
            return false;
        });
    }

    var upload = function (files) {
        var f = files[0];

//		 console.log(!f.type.match('image/jpeg'));
//		 console.log(!f.type.match('image/png'));

//		if (!f.type.match('image/jpeg') && !f.type.match('image/png')) {
//		}

        // if (f.type.match('image/jpeg')) {
        //     format = 'jpg';
        // } else
        if (f.type.match('image/png')) {
            format = 'png';
        } else if (f.name.split('.').pop().toLowerCase() === 'ttf') {
            format = 'ttf';
            filename = f.name;
        } else {
            var notif = new Notif();
            notif.add('<li>Le fichier doit avoir l\'extention .png ou .jpeg</li>');
            notif.show();
            return false;
        }

        var reader = new FileReader();

        reader.onload = handleReaderLoad;

        reader.readAsDataURL(f);
    }


    var handleReaderLoad = function (evt) {
        // var pic = {};
        // pic.file = evt.target.result.split(',')[1];
        // console.log(evt.target.result);

        // var str = jQuery.param(pic);

        // const form_data = new FormData();
        // form_data.append("file", evt.target.result);


        // console.log(str);

        $.ajax({
            type: 'POST',
            url: '/editor/upload/' + TOKEN,
            data: {data: evt.target.result},
            success: function (data, statut) {

                let elem = '';

                if (data === 'ERROR') {
                    return;
                }

                if (format === 'ttf') {
                    $('body').append('<div class="importFont" name="' + data + '" url="/editor/access/' + TOKEN + '/' + data + '"></div>');
                    $('head').append('<style>' +
                        '@font-face{' +
                        'font-family: "' + data + '";' +
                        'src: url("/editor/access/' + TOKEN + '/' + data + '" );' +
                        '}</style>');
                    elem = new FontElement(new Util().makeid(), 'font', 100, 100, data);
                } else {
                    elem = new ImgElement(new Util().makeid(), 'img', 100, 100, 'auto', 'auto', "/editor/access/" + TOKEN + '/' + data);
                }
                if ($('editor').attr('switcher') === 'workspaceOption') {
                    opt.addAndDraw(elem);
                } else {
                    workspace.addAndDraw(elem);
                }
                new Util().selected();
                format = '';
            },
            complete: function (data, statut) {
            }
        });
    }


});
