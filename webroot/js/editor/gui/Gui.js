var Gui = Panel.extend(function () {

    this.title = "";
    this.content = "";
    this.triggerElementClass = "";

    this.constructor = function (name, triggerElementClass, title, width, height) {
        this.super(name, 0, 0, width, height);
        this.title = title;
        this.triggerElementClass = triggerElementClass;
    }

    this.init = function () {
    }

    this.draw = function () {
    }

});