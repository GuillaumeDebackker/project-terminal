var HelpGui = Gui.extend(function () {

    var help = new HelpPanel();

    this.contructor = function (name, triggerElementClass, title, width, height) {
        this.super(name, triggerElementClass, title, width, height);

    }

    this.init = function () {
        this.content = '\<div class="guiContainer">\
							<div class="guiNav">\
								<div class="guiTitle"><i class="fa fa-question-circle" aria-hidden="true"></i><b> AIDE</b></div>\
								<ul id="helpscroll" style="height: 500px">\
									<li class="helpbtn button">Boutons</li>\
									<li class="helpbtn symbol">Symboles</li>\
									<li class="helpbtn text">Texte</li>\
									<li class="helpbtn champtext">Champ texte</li>\
									<li class="helpbtn rec">Rectangle</li>\
									<li class="helpbtn imghelp">Image</li>\
									<li class="helpbtn pghelp">Bar de progression</li>\
									<li class="helpbtn headhelp">Tête du joueur</li>\
								</ul>\
							</div>\
						<div class="guiDetail">\
							<h1 class="helpTitle">Menu d\'aide</h1>\
							<div class="helpDesc" style="padding-left: 20px">\
								Vous trouverez dans cette section de nombreuses informations sur les fonctionnalités de l\'éditeur de launcher.\
								 Sélectionnez un onglet à gauche sur le sujet qui vous intéresse.\
							</div>\
						</div>\
						</div>';
    }

    this.draw = function () {
        $(this.triggerElementClass).avgrund({
            width: this.width, // max is 640px
            height: this.height, // max is 350px
            showClose: false, // switch to 'true' for enabling close button
            showCloseText: '', // type your text for close button
            closeByEscape: true, // enables closing popup by 'Esc'..
            closeByDocument: true, // ..and by clicking document itself
            holderClass: '', // lets you name custom class for popin holder..
            overlayClass: '', // ..and overlay block
            enableStackAnimation: false, // enables different type of popin's animation
            onBlurContainer: '.workspace', // enables blur filter for specified block
            openOnEvent: true, // set to 'false' to init on load
            setEvent: 'click', // use your event like 'mouseover', 'touchmove', etc.
            template: this.content,
            onLoad: function (elem) {
                //$('.guiContainer').mCustomScrollbar();
                $('nav').hide("slide", {direction: "left"}, 300);
                $('.containerOpt').hide("slide", {direction: "right"}, 300);

//				$('.guiContainer').mCustomScrollbar();
            },
            onUnload: function (elem) {
                $('nav').show("slide", {direction: "left"}, 300);
                $('.containerOpt').show("slide", {direction: "right"}, 300);
            }
        });
    }

    this.addJQuery = function () {
        $('#helpscroll').mCustomScrollbar();
        $('.mCSB_inside > .mCSB_container').css('margin-right', '15px');
        $('.button').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES BOUTONS</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre bouton</div>');
            help.add('<div class="helpDesc"><span class="underlined">Action :</span>Action que doit réaliser votre bouton<br>Voici les différentes actions :\
					<ul class="listDesc">\
						<li>Aucune action</li>\
						<li>Fermer le launcher</li>\
						<li>Réduire le launcher</li>\
						<li>Ouvrir un lien</li>\
						<li>Lancer le jeu</li>\
						<li>Ouvrir le menu option</li>\
						<li>Fermer le menu option</li>\
						<li>Désinstaller le launcher</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre bouton<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Images :</span>Images affichées par le bouton<br>\
					<ul class="listDesc">\
						<li>De base</li>\
						<li>Hover (Affichée au survol du bouton)</li>\
					</ul>\
					<ul class="listDescInline">\
						<li><span class="underlined">De base</span><br><img src="' + RES + 'media/5.png"></li>\
						<li><span class="underlined">Hover</span><br><img src="' + RES + 'media/5_hover.png"></li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.symbol').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES SYMBOLES</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre symbole</div>');
            help.add('<div class="helpDesc"><span class="underlined">Action :</span>Action que doit réaliser votre symbole<br>Voici les différentes actions :\
					<ul class="listDesc">\
						<li>Aucune action</li>\
						<li>Hover</li>\
						<li>Fermer le launcher</li>\
						<li>Réduire le launcher</li>\
						<li>Ouvrir un lien</li>\
						<li>Lancer le jeu</li>\
						<li>Ouvrir le menu option</li>\
						<li>Fermer le menu option</li>\
						<li>Désinstaller le launcher</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre symbole<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
						<li>Taille (en px)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Couleur :</span>Couleur de votre symbole<br>Ajustez simplement la couleur grâce au sélécteur de couleur !<br>\
					<ul class="listDescInline">\
						<li><div id="helpPicker"></div></li>\
						<li class="faSymbolShow"><i class="fa fa-star" aria-hidden="true"></i></li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.text').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LE TEXTE</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre objet texte</div>');
            help.add('<div class="helpDesc"><span class="underlined">Action :</span>Action que doit réaliser votre texte<br>Voici les différentes actions :\
					<ul class="listDesc">\
						<li>Aucune action</li>\
						<li>Fermer le launcher</li>\
						<li>Réduire le launcher</li>\
						<li>Ouvrir un lien</li>\
						<li>Lancer le jeu</li>\
						<li>Hover</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre texte<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
						<li>Taille (en px)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Couleur :</span>Couleur de votre texte<br>Ajustez simplement la couleur grâce au sélécteur de couleur !<br>\
					<ul class="listDescInline">\
						<li><div id="helpPicker"></div></li>\
						<li class="faTextShow">Vous pouvez modifier la couleur de ce texte</i></li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Texte :</span>Changer en temps réel le texte<br>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.champtext').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES CHAMPS TEXTES</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre champ texte</div>');
            help.add('<div class="helpDesc"><span class="underlined">Action :</span>Action que doit réaliser votre champ texte<br>Voici les différentes actions :\
					<ul class="listDesc">\
						<li>Centrer le texte</li>\
						<li>Cacher le texte</li>\
						<li>Relier au pseudo</li>\
						<li>Relier au mot de passe</li>\
						<li>Relier à la RAM allouée</li>\
						<li>Relier à l\'emplacement de JAVA</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre champ texte<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Couleur :</span>Couleur de votre champ texte</div>');
            help.add('<div class="helpDesc"><span class="underlined">Background :</span>Couleur de fond votre champ texte</div>');
            help.add('<div class="helpDesc"><span class="underlined">Bordure :</span>Personaliser la bordure de votre champ texte<br>Voici les 2 paramètres modifiables :<br>\
					<ul class="listDesc">\
						<li>La couleur de la bordure (avec le sélécteur de couleur)</li>\
						<li>L\'opacité de la bordure (avec le slider)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Opacité :</span>Changez l\'opacité du background</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.rec').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES RECTANGLES</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre rectangle</div>');
            help.add('<div class="helpDesc"><span class="underlined">Action :</span>Action que doit réaliser votre rectangle<br>Voici les différentes actions :\
					<ul class="listDesc">\
						<li>Hover</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre rectangle<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coins arrondis :</span>Définissez des coins arrondis sur votre rectangle</div>');
            help.add('<div class="helpDesc"><span class="underlined">Couleur :</span>Couleur de fond</div>');
            help.add('<div class="helpDesc"><span class="underlined">Bordure :</span>Personaliser la bordure de votre rectangle<br>Voici les 2 paramètres modifiables :<br>\
					<ul class="listDesc">\
						<li>La couleur de la bordure (avec le sélécteur de couleur)</li>\
						<li>L\'opacité de la bordure (avec le slider)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Opacité :</span>Changez l\'opacité du background</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.imghelp').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES IMAGES</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre image</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre image<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.add('<div class="helpDesc"><span class="underlined">Image :</span>Modifier en temps réel l\'image');
            help.draw();
            help.addJQuery();
        });
        $('.pghelp').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES BARRES DE PROGRESSION</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Le nom de votre barre</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coordonnées :</span>Position et taille de votre barre<br>\
					<ul class="listDesc">\
						<li>X (horizontal)</li>\
						<li>Y (vertical)</li>\
						<li>Longueur (sur X)</li>\
						<li>Largeur (sur Y)</li>\
					</ul>\
					</div>');
            help.add('<div class="helpDesc"><span class="underlined">Coins arrondis :</span>Définissez des coins arrondis sur votre barre</div>');
            help.add('<div class="helpDesc"><span class="underlined">Couleur :</span>Couleur de fond</div>');
            /*help.add('<div class="helpDesc"><span class="underlined">Bordure :</span>Personaliser la bordure de votre barre<br>Voici les 2 paramètres modifiables :<br>\
             <ul class="listDesc">\
             <li>La couleur de la bordure (avec le sélécteur de couleur)</li>\
             <li>L\'opacité de la bordure (avec le slider)</li>\
             </ul>\
             </div>');*/
            help.add('<div class="helpDesc"><span class="underlined">Détails :</span>Changez le pourcentage de la barre</div>');
            help.add('<div class="helpDesc"><span class="underlined">Superposition :</span>Priorité d\'affichage par rapport aux autres éléments');
            help.draw();
            help.addJQuery();
        });
        $('.headhelp').click(function () {
            help.clear();
            help.add('<h1 class="helpTitle">LES TÊTES DE JOUEUR</h1>');
            help.add('<div class="helpDesc"><span class="underlined">Nom :</span>Pseudo pour tester le skin</div>');
            help.add('<div class="helpDesc"><span class="underlined">Infos :</span>La tête du joueur changera suivant le pseudo entré dans le champ text username</div>');
            help.draw();
            help.addJQuery();
        });
    }


});
