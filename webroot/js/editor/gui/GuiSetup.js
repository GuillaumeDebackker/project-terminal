;var GuiSetup = Gui.extend(function () {

    this.contructor = function (name, triggerElementClass, title, width, height) {
        this.super(name, triggerElementClass, title, width, height);
    }

    this.init = function () {
        $('body').append('<preconf launchx="0" launchy="0" launchbg="" hasopt="true" optx="0" opty="0" optbg="" bootx="0" booty="0" bootbg=""/>');
        this.content =
            '<div class="setupContainer ">\
                <div class="setup">\
                    <h1 class="setupTitle underlined"><i class="fa fa-cog" aria-hidden="true"></i> Préconfiguration du launcher</h1>\
                    <div class="setupDiv">Longueur du launcher en pixels : <input class="inputSetup" type="text" id="widthBg" name="width"></div>\
                    <div class="setupDiv">Largeur du launcher en pixels : <input class="inputSetup" type="text" id="heightBg" name="height"></div>\
                    <div class="setupDiv">Url de l\'image de fond : <input class="inputSetup" type="text" id="bgLauncher" name="bgLauncher"></div>\
                    <img id="demoLauncher" width=480 height=240 src="' + IMG + '1.png"/>\
                    <input class="inputBtnSetup" type="button" id="suivant" name="suivant" value="Suivant">\
                </div>\
                <div class="setup" style="top:0; left: 1200px; display: inline;">\
                    <h1 class="setupTitle underlined"><i class="fa fa-cog" aria-hidden="true"></i> Préconfiguration du menu option</h1>\
                    <div class="setupDiv">Longueur du menu option en pixels : <input class="inputSetup" type="text" id="widthBgOpt" name="widthOpt"></div>\
                    <div class="setupDiv">Largeur du menu option en pixels : <input class="inputSetup" type="text" id="heightBgOpt" name="heightOpt"></div>\
                    <div class="setupDiv">Url de l\'image de fond : <input class="inputSetup" type="text" id="bgLauncherOpt" name="bgLauncherOpt"></div>\
                    <img id="demoOption" width=480 height=240 src="' + IMG + '2.png"/>\
                    <input class="inputBtnSetup retour" type="button" id="retour" name="retour" value="Retour">\
                    <input class="nooption inputBtnSetup" type="button" name="next" value="Non merci">\
                    <input class="inputBtnSetup" type="button" id="suivant2" name="suivant2" value="Suivant">\
                </div>\
                <div class="setup" style="top:0; left: 2400px; display: inline;">\
                    <h1 class="setupTitle underlined"><i class="fa fa-cog" aria-hidden="true"></i> Préconfiguration du bootstrap</h1>\
                    <div class="setupDiv">Longueur du bootstrap en pixels : <input class="inputSetup" type="text" id="widthBgBoot" name="widthBoot"></div>\
                    <div class="setupDiv">Largeur du bootstrap en pixels : <input class="inputSetup" type="text" id="heightBgBoot" name="heightBoot"></div>\
                    <div class="setupDiv">Url de l\'image de fond : <input class="inputSetup" type="text" id="bgLauncherBoot" name="bgLauncherBoot"></div>\
                    <img id="demoBoot" width=500 height=280 src="' + IMG + '3.png"/>\
                    <input class="inputBtnSetup retour" type="button" id="retour2" name="retour" value="Retour">\
                    <input class="inputBtnSetup avgrund-close" type="button" id="finish" name="finish" value="Passer à l\'éditeur">\
                </div>\
            </div>';
    }

    this.draw = function () {
        $(document).avgrund({
            width: this.width, // max is 640px
            height: this.height, // max is 350px
            showClose: false, // switch to 'true' for enabling close button
            showCloseText: '', // type your text for close button
            closeByEscape: false, // enables closing popup by 'Esc'..
            closeByDocument: false, // ..and by clicking document itself
            holderClass: 'o', // lets you name custom class for popin holder..
            overlayClass: '', // ..and overlay block
            enableStackAnimation: false, // enables different type of popin's animation
            onBlurContainer: '.workspace', // enables blur filter for specified block
            openOnEvent: true, // set to 'false' to init on load
            setEvent: 'ready', // use your event like 'mouseover', 'touchmove', etc.
            template: this.content,
            onLoad: function (elem) {
                $('nav').hide("slide", {direction: "left"}, 300);
                $('.containerOpt').hide("slide", {direction: "right"}, 300);
            },
            onUnload: function (elem) {
                $('nav').show("slide", {direction: "left"}, 300);
                $('.containerOpt').show("slide", {direction: "right"}, 300);
            }
        });
    }

    this.addJQuery = function () {

        $('.setup').css('background', 'url("' + IMG + 'setupbg.jpg")');

        $('#widthBg').val(parseInt($('#demoLauncher').width()) * 2.5);
        $('#heightBg').val(parseInt($('#demoLauncher').height()) * 2.5);
        $('#bgLauncher').val($('#demoLauncher').attr('src'));

        $('preconf').attr('launchx', parseInt($('#demoLauncher').width()) * 2.5);
        $('preconf').attr('launchy', parseInt($('#demoLauncher').height()) * 2.5);
        $('preconf').attr('launchbg', $('#demoLauncher').attr('src'));

        $('preconf').attr('optx', parseInt($('#demoOption').width()) * 2.5);
        $('preconf').attr('opty', parseInt($('#demoOption').height()) * 2.5);
        $('preconf').attr('optbg', $('#demoOption').attr('src'));

        $('preconf').attr('bootx', parseInt($('#demoBoot').width()));
        $('preconf').attr('booty', parseInt($('#demoBoot').height()));
        $('preconf').attr('bootbg', $('#demoBoot').attr('src'));

        $('#widthBg').keyup(function () {
            $('#demoLauncher').css('width', parseInt($(this).val()) / 2.5 + 'px');
            $('preconf').attr('launchx', parseInt($(this).val()));
        });
        $('#heightBg').keyup(function () {
            $('#demoLauncher').css('height', parseInt($(this).val()) / 2.5 + 'px');
            $('preconf').attr('launchy', parseInt($(this).val()));
        });
        $('#bgLauncher').keyup(function () {
            $('#demoLauncher').attr('src', $(this).val());
            $('preconf').attr('launchbg', $(this).val());
        });
        $('#suivant').click(function () {
            $('.setup').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(-1200px, 0px)"
            });
            // var workspace = new WorkspacePanel();
            // workspace.setBackground($('#demoLauncher').attr('src'));
            // workspace.setBackground($('#demoLauncher').attr('src'));
        });

        $('#widthBgOpt').val(parseInt($('#demoOption').width()) * 2.5);
        $('#heightBgOpt').val(parseInt($('#demoOption').height()) * 2.5);
        $('#bgLauncherOpt').val($('#demoOption').attr('src'));

        $('#widthBgOpt').keyup(function () {
            $('#demoOption').css('width', parseInt($(this).val()) / 2.5 + 'px');
            $('preconf').attr('optx', parseInt($(this).val()));
        });
        $('#heightBgOpt').keyup(function () {
            $('#demoOption').css('height', parseInt($(this).val()) / 2.5 + 'px');
            $('preconf').attr('opty', parseInt($(this).val()));
        });
        $('#bgLauncherOpt').keyup(function () {
            $('#demoOption').attr('src', $(this).val());
            $('preconf').attr('optbg', $(this).val());
        });
        $('.nooption').click(function () {
            $('preconf').attr('hasopt', 'false');
            $('.setup').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(-2400px, 0px)"
            });
        });
        $('#suivant2').click(function () {
            $('preconf').attr('hasopt', 'true');
            $('.setup').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(-2400px, 0px)"
            });
            // var workspace = new WorkspacePanel();
            // workspace.setBackground($('#demoLauncher').attr('src'));
            // workspace.setBackground($('#demoLauncher').attr('src'));
        });

        $('#retour').click(function () {
            $('.setup').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(0px, 0px)"
            });
            // var workspace = new WorkspacePanel();
            // workspace.setBackground($('#demoLauncher').attr('src'));
            // workspace.setBackground($('#demoLauncher').attr('src'));
        });

        $('#widthBgBoot').val(parseInt($('#demoBoot').width()));
        $('#heightBgBoot').val(parseInt($('#demoBoot').height()));
        $('#bgLauncherBoot').val($('#demoBoot').attr('src'));

        $('#widthBgBoot').keyup(function () {
            // $('#demoBoot').css('width', parseInt($(this).val()) / 2.5  + 'px');
            $('#demoBoot').css('width', parseInt($(this).val()) + 'px');
            $('preconf').attr('bootx', parseInt($(this).val()));
        });
        $('#heightBgBoot').keyup(function () {
            $('#demoBoot').css('height', parseInt($(this).val()) + 'px');
            // $('#demoBoot').css('height', parseInt($(this).val()) / 2.5  + 'px');
            $('preconf').attr('booty', parseInt($(this).val()));
        });
        $('#bgLauncherBoot').keyup(function () {
            $('#demoBoot').attr('src', $(this).val());
            $('preconf').attr('bootbg', $(this).val());
        });

        $('#retour2').click(function () {
            $('.setup').css({
                'transition': '2s all ease',
                "-webkit-transform": "translate(-1200px, 0px)"
            });
            // var workspace = new WorkspacePanel();
            // workspace.setBackground($('#demoLauncher').attr('src'));
            // workspace.setBackground($('#demoLauncher').attr('src'));
        });

        $('#finish').click(function () {
            $('body').append('<setting name="' + NAME + '" version="1.7.2" premium="' + PREM + '" opt="' + OPT + '"></setting>');
//			$('body').removeClass("avgrund-active");
//            $('body').removeClass("avgrund-overlay");
            $('.workspace').css('width', $('preconf').attr('launchx') + 'px');
            $('.workspace').css('height', $('preconf').attr('launchy') + 'px');

            var height = $('.workspace').height()
            var width = $('.workspace').width()
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspace').css({
                "position": "absolute",
                "top": mTop,
                "left": mLeft,
                "width": width,
                "height": height,
                "background": "url('" + $('preconf').attr('launchbg') + "')",
                "background-size": "100% 100%"
            });
            $('.workspaceOption').css('height', $('preconf').attr('opty') + 'px');
            $('.workspaceOption').css('width', $('preconf').attr('optx') + 'px');
            var height = $('.workspaceOption').height()
            var width = $('.workspaceOption').width()
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspaceOption').css({
                "position": "absolute",
                "top": $(window).height() + ($(window).height() - $('.workspaceOption').height()) / 2 + 'px',
                "left": mLeft,
                "width": width,
                "height": height,
                "background": "url('" + $('preconf').attr('optbg') + "')",
                "background-size": "100% 100%"
            });
            $('.workspaceBoot').css('height', $('preconf').attr('booty') + 'px');
            $('.workspaceBoot').css('width', $('preconf').attr('bootx') + 'px');
            var height = $('.workspaceBoot').height()
            var width = $('.workspaceBoot').width()
            var mTop = $(window).height() / 2 - height / 2;
            var mLeft = $(window).width() / 2 - width / 2;
            $('.workspaceBoot').css({
                "position": "absolute",
                "top": $(window).height() * 2 + ($(window).height() - $('.workspaceBoot').height()) / 2 + 'px',
                "left": mLeft,
                "width": width,
                "height": height,
                "background": "url('" + $('preconf').attr('bootbg') + "')",
                "background-size": "100% 100%"
            });
            //"top" : $(window).height()*2 + ($(window).height() - $('.workspaceBoot').height()) / 2 +'px',
            // var workspace = new WorkspacePanel();
            // workspace.setBackground($('#demoLauncher').attr('src'));
            // workspace.setBackground($('#demoLauncher').attr('src'));

            let introguide = introJs();
            introguide.setOptions({
                steps: [
                    {
                        element: '.workspace',
                        intro: '<h3>Bienvenue sur l\'éditeur de launchers en ligne</h3>Dans cette zone vous pourrez configurer votre launcher.',
                        position: 'right'
                    },
                    {
                        element: '.button',
                        intro: '<h3>Eléments à votre disposition</h3>Pour concevoir votre launcher divers boutons sont à votre disposition.',
                        position: 'bottom'
                    },
                    {
                        element: '.faIcon',
                        intro: '<h3>Eléments à votre disposition</h3>Vous pourrez également profiter toutes sortes d\'icones.',
                        position: 'bottom'
                    },
                    {
                        element: '.fonts',
                        intro: '<h3>Eléments à votre disposition</h3>De nombreuses polices de caractères peuvent etre ajouté.',
                        position: 'bottom'
                    },
                    {
                        element: '.setting',
                        intro: '<h3>Paramètres</h3>Dans cet onglet il est possible de modifier la version de minecraft, l\'image de fond du launcher et bien plus encore...',
                        position: 'bottom'
                    },
                    {
                        element: '.save',
                        intro: '<h3>Sauvegarde</h3>Il est possible de sauvegarder manuellement ici. Toutefois l\'éditeur est configuré pour sauvegarder automatiquement toutes les 5 minutes',
                        position: 'bottom'
                    },
                    {
                        element: '.export',
                        intro: '<h3>Exporter le launcher</h3>Ce bouton permet d\'obtenir le launcher en .jar et de l\'excuter depuis votre ordinateur.',
                        position: 'bottom'
                    },
                    {
                        element: '.help',
                        intro: '<h3>Menu d\'aide</h3>Si vous avez besoin d\'information cliquez ici.',
                        position: 'bottom'
                    },
                    {
                        element: '.arbo',
                        intro: '<h3>Arborescence des fichiers</h3>Tous les éléments présent sur le launcher sont afficher ici. Les raccourcis clavier fonctionnent aussi ici.',
                        position: 'bottom'
                    },
                    {
                        element: '.launcher',
                        intro: '<h3>Launcher</h3>Cliquer ici pour accéder au plan de travail du launcher.',
                        position: 'bottom'
                    },
                    {
                        element: '.bootstrap',
                        intro: '<h3>Bootstrap</h3>Cliquer ici pour accéder au plan de travail du bootstrap.',
                        position: 'bottom'
                    },
                    {
                        element: '.trash',
                        intro: '<h3>Corbeille</h3>Pour supprimer un élément déglissez le sur la corbeille. Vous pouvez aussi appuiez sur la touche "suppr" de votre clavier.',
                        position: 'bottom'
                    },
                    {
                        element: '.switchopt',
                        intro: '<h3>Cacher le menu option</h3>Si le menu option vous géne, il est possible de le bloquer cacher avec cette option.',
                        position: 'bottom'
                    },
                    {
                        element: '.workspace',
                        tooltipClass: 'tooltipClassWidthFinal',
                        intro: '<h3>Raccourci clavier</h3>' +
                        'L\'éditeur contient quelques raccoucis clavier :<ul>' +
                        '<li><strong>Ctrl + s :</strong> sauvegarde le launcher</li>' +
                        '<li><strong>Suppr :</strong> supprime l\'élément sélectionné</li>' +
                        '<li><strong>Molette sur les champs texte :</strong> augmente / diminue la valeur de + 1 ou -1</li>' +
                        '<li><strong>Flèches directionnelles :</strong> bouge l\'élément sélectionné de 1</li>' +
                        '<li><strong>Flèches directionnelles + maj :</strong> bouge l\'élément sélectionné de 2</li>' +
                        '</ul>',
                        position: 'auto'
                    },
                    {
                        element: '.workspace',
                        tooltipClass: 'tooltipClassWidthFinal',
                        intro: '<h3>Upload d\'images</h3>' +
                        'Il est possible d\'upload des images directement sur le launcher. Pour cela il vous suffit de glisser et déposer votre image dans la fenêtre de l\'éditeur. Attention uniquement le format .png est supporté.',
                        position: 'auto'
                    },
                ],
                tooltipPosition : 'middle-middle'
            });
            introguide.start();
        });
    }


});
