var ImgElement = Element.extend(function () {

    this.constructor = function (id, n, x, y, w, h, src) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.src = src;
    }

    this.toString = function () {
        return '<img class="element ImgElement id' + this.id + '" name="' + this.name + '" src="' + this.src + '" type="ImgElement" width="200" height="200"/>';
    }

    this.addJQuery = function () {
        $('.id' + this.id).draggable();
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        this.addConfig2();
    }

    this.addConfig2 = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('Z');
            opt.addFunction('SRC');
            opt.addFunction('CONVERT');

            opt.draw();
            opt.addActionJQuery();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
        return opt;
    }


});
