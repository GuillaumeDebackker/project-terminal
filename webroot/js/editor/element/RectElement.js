var RectElement = Element.extend(function () {

    this.constructor = function (id, n, x, y, w, h) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    this.toString = function () {
        return '<div class="element RectElement id' + this.id + '" name="' + this.name + '" type="RectElement" hover="rgb(0, 0, 0)" hoverset="0"></div>';
    }

    this.addJQuery = function () {
        $('.id' + this.id).mouseenter(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('background-color'));
                $(this).css('background-color', hovering);
            }
        });
        $('.id' + this.id).mouseleave(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('background-color'));
                $(this).css('background-color', hovering);
            }
        });
        $('.id' + this.id).css('border-style', 'solid');
        $('.id' + this.id).css('border-width', '0px');
        $('.id' + this.id).css('border-color', 'rgba(255, 255, 255, 1)');
        this.super.addJQuery();
    }

    this.setBackground = function (color) {
        $('.id' + this.id).css('background', color);
    }

    this.setBordersize = function (size) {
        $('.id' + this.id).css('border-width', size + 'px');
    }

    this.setBordercolor = function (color) {
        $('.id' + this.id).css('border-color', color);
    }

    this.addConfig = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            opt.addAction(['hover'], ['hover']);
            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('ARROND');
            opt.addFunction('COLOR');
            opt.addFunction('BORDER');
            opt.addFunction('ALPHA');
            opt.addFunction('Z');

            opt.draw();
            opt.addJQueryColor('background');
            opt.addJQueryBorderColor('border-color');
            opt.addActionJQuery();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
        $('.id' + this.id).resizable({
            handles: 'n, e, s, w, ne, sw, se, nw',
            resize: function () {
                opt.update($(this));
            }
        });

    }

});