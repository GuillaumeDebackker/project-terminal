var FontElement = Element.extend(function () {

    this.font = '';

    this.constructor = function (id, n, x, y, font) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = 'auto';
        this.height = 'auto';
        this.font = font;
    }

    this.toString = function () {
        return '<div class="element FontElement id' + this.id + '" name="' + this.name + '" style="font-family: \'' + this.font + '\'; font-size: 3em; color:black;" type="FontElement" hover="black" hoverset="0" action="noaction">Kelix</div>';
    }

    this.addJQuery = function () {
        $('.id' + this.id).mouseenter(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('color'));
                $(this).css('color', hovering);
            }
        });
        $('.id' + this.id).mouseleave(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('color'));
                $(this).css('color', hovering);
            }
        });
        this.super.addJQuery2()
    }

    this.addConfig2 = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            this.action = ['noaction', 'close', 'red', 'link', 'start', 'hover'];
            this.desc = ['Aucune action', 'Ferme le launcher', 'Réduit le launcher', 'Ouvrir un lien', 'Lance le jeu', 'Hover'];
            opt.addAction(this.action, this.desc);
            opt.addFunction('TEXT');
            opt.addFunction('COORDWT', '<h3>Coordonnées</h3><br>');
            opt.addFunction('COLOR');
            opt.addFunction('FONTSELECT');
            opt.addFunction('Z');

            opt.draw();
            opt.addJQueryColor();
            opt.addActionJQuery();
            opt.addJQueryFont();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
    }

    this.setSize = function (size) {
        $('.id' + this.id).css('font-size', size + 'px');
    }

    this.setText = function (text) {
        $('.id' + this.id).text(text);
    }

    this.setFontFamily = function (font) {
        $('.id' + this.id).css('font-family', font);
    }
});
