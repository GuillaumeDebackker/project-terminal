var HeadElement = Element.extend(function () {

    this.constructor = function (id, n, x, y, w, h, head) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.head = head;
    }

    this.toString = function () {
        return '<img class="element HeadElement id' + this.id + '" name="DevQuentin" src="https://minotar.net/avatar/' + this.head + '/200" head="' + this.head + '" width="200" height="200" type="HeadElement"/>';
    }

    this.addJQuery = function () {
        // $('.id' + this.id).draggable();
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        this.addConfig2();
        this.drag();
    }

});
