var TextfieldElement = Element.extend(function () {

    this.constructor = function (id, n, x, y, w, h) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    this.toString = function () {
        return '<input type="text" class="element TextfieldElement id' + this.id + '" name="' + this.name + '" autocomplete="off" type="TextfieldElement">';
    }

    this.addJQuery = function () {
        $('.id' + this.id).css('position', 'none');
        $('.id' + this.id).draggable();
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        $('.id' + this.id).css('border-style', 'solid');
        $('.id' + this.id).css('border-width', '0px');
        $('.id' + this.id).css('border-color', 'rgba(0, 0, 0, 1)');
        this.addConfig();
    }

    this.addConfig = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            this.action = ['center', 'hide', 'pseudo', 'password', 'ram', 'javaPath'];
            this.desc = ['Center le texte', 'Cacher le texte', 'Relier au pseudo', 'Relier au mot de passe', 'Relier à la RAM allouée', 'Relier à l\'emplacement de JAVA'];
            opt.addAction(this.action, this.desc);

            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('COLOR');
            opt.addFunction('BACKGROUND');
            opt.addFunction('BORDER');
            opt.addFunction('ALPHA');
            opt.addFunction('Z');

            opt.draw();

            opt.addActionJQuery();
            opt.addJQueryColor();
            opt.addJQueryBackground();
            opt.addJQueryTextfield();
            opt.addJQueryBorderColor();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
    }

    this.setColor = function (color) {
        $('.id' + this.id).css('color', color);
    }

    this.setBackground = function (color) {
        $('.id' + this.id).css('background', color);
    }

    this.setType = function (type) {
        $('.id' + this.id).attr('type', type);
    }

    this.setTextAlign = function (align) {
        $('.id' + this.id).css('text-align', align);
    }

    this.setBordersize = function (size) {
        $('.id' + this.id).css('border-width', size + 'px');
    }

    this.setBordercolor = function (color) {
        $('.id' + this.id).css('border-color', color);
    }

});
