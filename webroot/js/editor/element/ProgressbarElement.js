var ProgressbarElement = Element.extend(function () {

    this.constructor = function (id, n, x, y, w, h) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    this.toString = function () {
        return '<div class="element ProgressbarElement id' + this.id + '" name="' + this.name + '" style="width: 500px; height: 10px; background: #FFF" type="ProgressbarElement"><div class="in-bar element" style="width:50%; height:100%; background: #EC8300;" ></div></div>';
    }

    this.addJQuery = function () {
        $('.id' + this.id).draggable();
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        $('.id' + this.id).css('border-style', 'solid');
        $('.id' + this.id).css('border-width', '0px');
        $('.id' + this.id).css('border-color', 'rgba(255, 255, 255, 1)');
        this.super.addConfig();
        this.addConfig2();
    }

    this.addConfig2 = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('COLOR');
            opt.addFunction('BACKGROUND');
            opt.addFunction('PGBPERCENT');
            opt.addFunction('ARROND');
            //opt.addFunction('BORDER');
            opt.addFunction('Z');

            opt.draw();
            opt.addJQueryProgress();
            opt.addJQueryBackground();
            //opt.addJQueryBorderColor();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
        return opt;
    }

    this.setBackground = function (color) {
        $('.id' + this.id).css('background', color);
    }

    this.setColor = function (color) {
        $('.id' + this.id + ' .in-bar').css('background', color);
    }

    this.setProgress = function (val) {
        $('.id' + this.id + ' .in-bar').css('width', val);
    }

    this.setBorderradius = function (val) {
        $('.id' + this.id).css('border-radius', val);
        $('.id' + this.id + ' .in-bar').css('border-radius', val);
    }

    this.setBordersize = function (size) {
        $('.id' + this.id).css('border-width', size + 'px');
    }

    this.setBordercolor = function (color) {
        $('.id' + this.id).css('border-color', color);
    }
});