var Element = Class.extend(function () {

    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.width = 0;
    this.height = 0;
    this.id = 0;
    this.name = "";

    /** Constructeur d'un élément */
    this.constructor = function (id, name) {
        this.id = id;
        this.name = name;
    }

    /** Retourne la position X */
    this.getX = function () {
        return $('.id' + id).position().left;
    }

    /** Retourne la position Y */
    this.getY = function () {
        return $('.id' + id).position().top;
    }

    /** Retourne la position Z */
    this.getZ = function () {
        return this.z;
    }

    /** Retourne la longueur */
    this.getWidth = function () {
        return $('.id' + id).width();
    }

    /** Retourne la largeur */
    this.getHeight = function () {
        return $('.id' + id).height();
    }

    /** Retourne l'id */
    this.getId = function () {
        return this.id;
    }

    /** Retourne le nom */
    this.getName = function () {
        return $('.id' + this.id).attr('name');
    }

    /** Définir la position X */
    this.setX = function (x) {
        this.x = x;
    }

    /** Définir la position Y */
    this.setY = function (y) {
        this.y = y;
    }

    /** Définir la position Z */
    this.setZ = function (z) {
        this.z = z;
    }

    /** Définir la longueur */
    this.setWidth = function (width) {
        this.width = width;
    }

    /** Définir la largeur */
    this.setHeight = function (height) {
        this.height = height;
    }

    /** Définir le nom */
    this.setName = function (name) {
        this.name = name;
    }

    /** Retourne la string de rendu */
    this.toString = function () {
        return "";
    }

    /** Retourne le type de l'element */
    this.getType = function () {
        return $('.id' + this.id).attr('type');
    }

    this.addJQuery = function () {
        $('.id' + this.id).draggable({
            // start: function (event, ui) {
            // 	startX = event.clientX;
            // 	startY = event.clientY;
            // },
            drag: function (event, ui) {
                // Remove any pre-existing drag references
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();

                // console.log('PASSED');

                var onDrag = $(this);
                var toDrag = "";

                $('.element').each(function (i, item) {
                    var $elem = $(item);
                    var itemY = $elem.position().top;
                    if (onDrag.position().top - 10 < itemY && itemY < onDrag.position().top + 10 && $elem.attr('class') != onDrag.attr('class')) {
                        var $referenceV = $('<div>', {'class': 'drag-reference-h'});
                        var $container = $elem.parent();
                        $referenceV.insertBefore($elem);
                        $referenceV.css({
                            top: $elem.position().top,
                            left: 0,
                            width: $container.width()
                        });
                        toDrag = $elem;
                        // onDrag.css('top', $elem.position().top + "px");
                    }
                });

                // console.log(event.buttons);
                // if (event.buttons == 1) {
                // 	// var vectX = event.clientX - startX;
                // 	// var vectY = event.clientY - startY;
                // 	var norme = Math.sqrt(Math.pow(event.clientX - startX, 2) + Math.pow(event.clientY - startY, 2));
                // 	console.log(norme);
                // 	if (norme < 2) {
                // 		if (toDrag != "") {
                // 			onDrag.css('top', toDrag.position().top + "px");
                // 			toDrag = "";
                // 			return false;
                // 		}
                // 	}
                // }
                // var e =


            },
            stop: function (event, ui) {
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();
            },
            // containment: 'parent',
            snap: '.drag-reference-v, .drag-reference-h, .element'
        });
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).resizable({
            handles: 'n, e, s, w, ne, sw, se, nw'
        });
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        this.addConfig();
    }

    this.addJQuery2 = function () {
        $('.id' + this.id).draggable({
            // start: function (event, ui) {
            // 	startX = event.clientX;
            // 	startY = event.clientY;
            // },
            drag: function (event, ui) {
                // Remove any pre-existing drag references
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();

                // console.log('PASSED');

                var onDrag = $(this);
                var toDrag = "";

                $('.element').each(function (i, item) {
                    var $elem = $(item);
                    var itemY = $elem.position().top;
                    if (onDrag.position().top - 10 < itemY && itemY < onDrag.position().top + 10 && $elem.attr('class') != onDrag.attr('class')) {
                        var $referenceV = $('<div>', {'class': 'drag-reference-h'});
                        var $container = $elem.parent();
                        $referenceV.insertBefore($elem);
                        $referenceV.css({
                            top: $elem.position().top,
                            left: 0,
                            width: $container.width()
                        });
                        toDrag = $elem;
                        // onDrag.css('top', $elem.position().top + "px");
                    }
                });

                // console.log(event.buttons);
                // if (event.buttons == 1) {
                // 	// var vectX = event.clientX - startX;
                // 	// var vectY = event.clientY - startY;
                // 	var norme = Math.sqrt(Math.pow(event.clientX - startX, 2) + Math.pow(event.clientY - startY, 2));
                // 	console.log(norme);
                // 	if (norme < 2) {
                // 		if (toDrag != "") {
                // 			onDrag.css('top', toDrag.position().top + "px");
                // 			toDrag = "";
                // 			return false;
                // 		}
                // 	}
                // }
                // var e =


            },
            stop: function (event, ui) {
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();
            },
            // containment: 'parent',
            snap: '.drag-reference-v, .drag-reference-h, .element'
        });
        $('.id' + this.id).attr("tabindex", -1);
        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        this.addConfig2();
    }

    this.addConfig = function () {
        var opt = this.addConfig2();
        $('.id' + this.id).resizable({
            handles: 'n, e, s, w, ne, sw, se, nw',
            resize: function () {
                opt.update($(this));
            }
        });
    }

    this.addConfig2 = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('Z');

            opt.draw();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
        return opt;
    }

    this.setAction = function (action) {
        $('.id' + this.id).attr('action', action);
    }

    this.setSize = function (size) {
        $('.id' + this.id).css('font-size', size + 'px');
    }

    this.setColor = function (color) {
        $('.id' + this.id).css('color', color);
    }

    this.setColorHover = function (color) {
        $('.id' + this.id).attr('hover', color);
    }

    this.setColorHoverActif = function (hover) {
        $('.id' + this.id).attr('hoverset', hover);
    }

    this.setZ = function (z) {
        $('.id' + this.id).css('z-index', z);
    }

    this.setBackground = function (color) {
        $('.id' + this.id).css('background', color);
    }

    this.setOpacity = function (opacity) {
        $('.id' + this.id).css('opacity', opacity);
    }

    this.setArrondCorner = function (px) {
        $('.id' + this.id).css('boder-radius', px + 'px');
    }

    this.drag = function () {
        $('.id' + this.id).draggable({
            // start: function (event, ui) {
            // 	startX = event.clientX;
            // 	startY = event.clientY;
            // },
            drag: function (event, ui) {
                // Remove any pre-existing drag references
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();

                // console.log('PASSED');

                var onDrag = $(this);
                var toDrag = "";

                $('.element').each(function (i, item) {
                    var $elem = $(item);
                    var itemY = $elem.position().top;
                    if (onDrag.position().top - 10 < itemY && itemY < onDrag.position().top + 10 && $elem.attr('class') != onDrag.attr('class')) {
                        var $referenceV = $('<div>', {'class': 'drag-reference-h'});
                        var $container = $elem.parent();
                        $referenceV.insertBefore($elem);
                        $referenceV.css({
                            top: $elem.position().top,
                            left: 0,
                            width: $container.width()
                        });
                        toDrag = $elem;
                        // onDrag.css('top', $elem.position().top + "px");
                    }
                });

                // console.log(event.buttons);
                // if (event.buttons == 1) {
                // 	// var vectX = event.clientX - startX;
                // 	// var vectY = event.clientY - startY;
                // 	var norme = Math.sqrt(Math.pow(event.clientX - startX, 2) + Math.pow(event.clientY - startY, 2));
                // 	console.log(norme);
                // 	if (norme < 2) {
                // 		if (toDrag != "") {
                // 			onDrag.css('top', toDrag.position().top + "px");
                // 			toDrag = "";
                // 			return false;
                // 		}
                // 	}
                // }
                // var e =


            },
            stop: function (event, ui) {
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();
            },
            containment: 'parent',
            snap: '.drag-reference-v, .drag-reference-h, .element'
        });
    }

});
