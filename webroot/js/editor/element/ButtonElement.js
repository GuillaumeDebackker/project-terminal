var ButtonElement = Element.extend(function () {

    this.img = '';
    this.img_hover = '';
    this.action = '';


    this.constructor = function (id, n, x, y, w, h, img, img_hover) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.img = img;
        this.img_hover = img_hover;
    }

    this.setAction = function (action) {
        $('.id' + this.id).attr('action', action);
    }

    this.toString = function () {
        return '<img class="element ButtonElement id' + this.id + '" name="' + this.name + '" src="' + this.img + '" base="' + this.img + '" data-alt-src="' + this.img_hover + '" type="ButtonElement" />';

    }

    var sourceSwap = function () {
        var $this = $(this);
        var newSource = $this.data('alt-src');
        $this.data('alt-src', $this.attr('src'));
        $this.attr('src', newSource);
    }

    this.addJQuery = function () {
        // var startX, startY = 0;
        $('.id' + this.id).draggable({
            // start: function (event, ui) {
            // 	startX = event.clientX;
            // 	startY = event.clientY;
            // },
            drag: function (event, ui) {
                // Remove any pre-existing drag references
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();

                // console.log('PASSED');

                var onDrag = $(this);
                var toDrag = "";

                $('.element').each(function (i, item) {
                    var $elem = $(item);
                    var itemY = $elem.position().top;
                    if (onDrag.position().top - 10 < itemY && itemY < onDrag.position().top + 10 && $elem.attr('class') != onDrag.attr('class')) {
                        var $referenceV = $('<div>', {'class': 'drag-reference-h'});
                        var $container = $elem.parent();
                        $referenceV.insertBefore($elem);
                        $referenceV.css({
                            top: $elem.position().top,
                            left: 0,
                            width: $container.width()
                        });
                        toDrag = $elem;
                        // onDrag.css('top', $elem.position().top + "px");
                    }
                });

                // console.log(event.buttons);
                // if (event.buttons == 1) {
                // 	// var vectX = event.clientX - startX;
                // 	// var vectY = event.clientY - startY;
                // 	var norme = Math.sqrt(Math.pow(event.clientX - startX, 2) + Math.pow(event.clientY - startY, 2));
                // 	console.log(norme);
                // 	if (norme < 2) {
                // 		if (toDrag != "") {
                // 			onDrag.css('top', toDrag.position().top + "px");
                // 			toDrag = "";
                // 			return false;
                // 		}
                // 	}
                // }
                // var e =


            },
            stop: function (event, ui) {
                $('.drag-reference-v').remove();
                $('.drag-reference-h').remove();
            },
            // containment: 'parent',
            snap: '.drag-reference-v, .drag-reference-h, .element'
        });
        $('.id' + this.id).attr("tabindex", -1);
        //$('.id' + this.id).hover(sourceSwap, sourceSwap);

        $('.id' + this.id).mouseenter(function () {
            var data = $(this).attr('data-alt-src');
            var src = $(this).attr('src');
            $(this).attr('src', data);
            $(this).attr('data-alt-src', src);
        }).mouseleave(function () {
            var data = $(this).attr('data-alt-src');
            var src = $(this).attr('src');
            $(this).attr('src', data);
            $(this).attr('data-alt-src', src);
        });

        $('.id' + this.id).css("left", this.x);
        $('.id' + this.id).css("top", this.y);
        $('.id' + this.id).css("width", this.width);
        $('.id' + this.id).css("height", this.height);
        this.addConfig();
    }

    this.addConfig = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');

            this.action = ['close', 'red', 'link', 'start', 'openOpt', 'closeOpt', 'uninstall'];
            this.desc = ['Ferme le launcher', 'Réduit le launcher', 'Ouvrir un lien', 'Lance le jeu', 'Ouvrir le menu option', 'Ferme le menu option', 'Déinstallation du launcher'];
            opt.addAction(this.action, this.desc);
            opt.addFunction('COORD', '<h3>Coordonnées</h3><br>');
            opt.addFunction('BASE');
            opt.addFunction('Z');

            opt.draw();
            opt.addActionJQuery();

            opt.init($(this));
        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
    }
});
