var FaElement = Element.extend(function () {

    this.faIcon = '';

    this.constructor = function (id, n, x, y, faIcon) {
        this.id = id;
        this.name = n;
        this.x = x;
        this.y = y;
        this.width = 'auto';
        this.height = 'auto';
        this.faIcon = faIcon;
    }

    this.toString = function () {
        return '<i class="element FaElement id' + this.id + ' item' + this.faIcon + ' fa" name="' + this.name + '"iconid="' + this.faIcon + '" hover="rgb(0, 0, 0)" hoverset="0" type="FaElement" action="noaction"></i>';
    }

    this.addJQuery = function () {
        $('.id' + this.id).mouseenter(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('color'));
                $(this).css('color', hovering);
            }
        });
        $('.id' + this.id).mouseleave(function () {
            if ($(this).attr('hoverset') == "1") {
                var hovering = $(this).attr('hover');
                $(this).attr('hover', $(this).css('color'));
                $(this).css('color', hovering);
            }
        });
        $('.id' + this.id).css('font-size', '30px');
        this.super.addJQuery2();
    }

    this.addConfig2 = function () {
        var opt = new OptionPanel();
        $('.id' + this.id).mousedown(function () {

            opt.clear();

            opt.addFunction('NAME');
            this.action = ['noaction', 'hover', 'close', 'red', 'link', 'start', 'openOpt', 'closeOpt', 'uninstall'];
            this.desc = ['Aucune action', 'Hover', 'Ferme le launcher', 'Réduit le launcher', 'Ouvrir un lien', 'Lance le jeu', 'Ouvrir le menu option', 'Ferme le menu option', 'Déinstallation du launcher'];
            opt.addAction(this.action, this.desc);
            opt.addFunction('COORDWT', '<h3>Coordonnées</h3><br>');
            opt.addFunction('COLOR');
            opt.addFunction('ALPHAFA')
            opt.addFunction('Z');

            opt.draw();
            opt.addJQueryColor();
            opt.addActionJQuery();
            opt.init($(this));

        });
        $('.id' + this.id).bind('drag', function () {
            opt.update($(this));
        });
    }

});
