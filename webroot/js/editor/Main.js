//MAIN


var sourceSwap = function () {
    var $this = $(this);
    var newSource = $this.data('alt-src');
    $this.data('alt-src', $this.attr('src'));
    $this.attr('src', newSource);
}


$(document).ready(function (){

    // console.log(COOKIE);
    SOCKET.emit('AUTH', COOKIE);

    // SOCKET.emit('LOGIN', {cookie: COOKIE });


    //PRE-INIT

    var workspace = new WorkspacePanel('workspace', 100, 100, 1000, 500);
    var opt = new WorkspaceOptPanel('workspaceOption', 100, 100, 1000, 500);
    var boot = new WorkspaceBootPanel('workspaceBoot', 100, 100, 700, 300);

    var button = new ItemNavbar(1, "button", "fa-picture-o");
    var faIcon = new ItemNavbar(2, "faIcon", "fa-square-o");
    var font = new ItemNavbar(3, "fonts", "fa-font");
    var tool = new ItemNavbar(4, "tools", "fa-pencil-square");
    var setting = new ItemNavbar(5, "setting", "fa-gears");
    var help = new ItemNavbar(6, "help", "fa-question").setShowmenuitems(false);
    var arbo = new ItemNavbar(8, "arbo", "fa-arrow-right").setShowmenuitems(false);

    var settingPanel = new SettingPanel(setting.getId());
    settingPanel.addAction('name', 'Nom du serveur', 'TEXTFIELD');
    settingPanel.addAction('premium', 'Premium', 'TOGGLE');
    settingPanel.addAction('background', 'Image de fond', 'BACKGROUND');
    settingPanel.addAction('width', 'Longeur du fond', 'WIDTH');
    settingPanel.addAction('height', 'Largeur du fond', 'HEIGHT');
    settingPanel.addAction('settingOpt', 'Menu option', 'OPT');
    settingPanel.addAction('forge', 'Forge', 'FORGE');
    settingPanel.addAction('version', 'Version', 'SELECT');
    settingPanel.addAction('link_update', 'Lien de mise à jour', 'LINK_UPDATE');

    // var arboPanel = new OrderPanel();

    data = new Util().readJSON(RES + 'media.json');
    var style = '<style>';
    for (var i in data.fonts) {
        style += '@font-face{font-family: "' + data.fonts[i].name + '";src: url("' + RES + data.fonts[i].url + '");}';
        font.add('<li class="fontDisp" style="font-family: \'' + data.fonts[i].name + '\';" name="' + data.fonts[i].name + '">Kelix</li>');
    }
    $('head').append(style + '</style>');

    for (var i in data.img) {
        button.add('<li><img class="drawimg" name="' + data.img[i].name + '" src="' + RES + data.img[i].icon_url + '" img_url="' + RES + data.img[i].img_url + '" data-alt-src="' + RES + data.img[i].img_url_hover + '" alt="' + data.img[i].name + '" /></li>');
    }


    indexOf = [].indexOf || function (item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item)
                    return i;
            }
            return -1;
        };
    var scriptObj = '<style media="screen">';
    var recurrent = [61455, 61471, 61472, 61503, 61519, 61535,
        61551, 61567, 61583, 61599, 61615, 61619, 61620,
        61621, 61622, 61623, 61624, 61625, 61626, 61627,
        61628, 61629, 61630, 61631, 61647, 61663, 61679,
        61695, 61711, 61718, 61719, 61727, 61743, 61759,
        61775, 61791, 61807, 61823, 61839, 61855, 61871,
        61887, 61903, 61919, 61935, 61951];
    for (var i = 61440; i <= 61964; i++) {
        if (indexOf.call(recurrent, i) < 0) {
            hexString = "\\" + i.toString(16);
            scriptObj += '.item' + i + ':before{content: "' + hexString + '"; font-family: FontAwesome;}';
            faIcon.add('<i class="fa allItem item' + i + '" name="' + i + '" id="' + i + '"></i>');
        }
    }
    scriptObj += '</style>';
    $('head').append(scriptObj);

    tool.add('<li class="itemTextfield"><i class="fa fa-pencil-square-o"></i> Champ text</li>');
    tool.add('<li class="itemRect"><i class="fa fa-square"></i> Rectangle</li>');
    tool.add('<li class="itemImg"><i class="fa fa-file-image-o"></i> Image custom</li>');
    tool.add('<li class="itemPB"><i class="fa fa-spinner"></i> Bar de Progression</li>');
    tool.add('<li class="itemHead"><i class="fa fa-smile-o"></i> Tête du joueur</li>');
    tool.add('<li class="itemBC"><i class="fa fa-indent"></i> Bouttons custom</li>');


    //ADD ELEMENT
    var navbar = new NavbarPanel();
    navbar.add(button);
    navbar.add(faIcon);
    navbar.add(font);
    navbar.add(tool);
    navbar.add(setting);
    navbar.add(new ItemNavbar(6, "save", "fa-floppy-o").setShowmenuitems(false));
    navbar.add(new ItemNavbar(7, "export", "fa-share-square-o").setShowmenuitems(false));
    navbar.add(help);
    navbar.add(arbo);


    //GUI

    var helpgui = new HelpGui("helpgui", '.help', "Aide", 1200, 600);

    helpgui.init();


    //RENDERING

    navbar.draw();
    button.draw();
    faIcon.draw();
    font.draw();
    tool.draw();
    setting.drawPanel(settingPanel);
    settingPanel.addJQuery();

    arbo.setAtBottom();
    arbo.drawPanel(orderp);
    orderp.addJQuery();

    helpgui.draw();

    settingPanel.addAction('background', 'Image de fond', 'BACKGROUND');
    workspace.draw();
    opt.draw();
    boot.draw();


    workspace.clearElements();
    navbar.clearElements();


//	$('body').append('<setting name="" version="" premium=""></setting>');
    $('body').append('<editor switcher="workspace"></editor>');

    // var load = new Load();
    // load.loadLauncher(null, null, null);

    // settingPanel.setBg($('.textfield2 .background'), $('.workspace').css('background-image'));
    // settingPanel.update($('.textfield2 .width'), $('.workspace').css('width'));
    // settingPanel.update($('.textfield2 .height'), $('.workspace').css('height'));


    // ACTION JQUERY

    $('.allItem').click(function () {
        var elem = new FaElement(new Util().makeid(), 'fa', 100, 100, $(this).attr('name'));
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
//		arboPanel.add(elem);
    });
    $('.drawimg').click(function () {
        var elem = new ButtonElement(new Util().makeid(), 'button', 100, 100, 'auto', 'auto', $(this).attr('img_url'), $(this).attr('data-alt-src'))
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
//		arboPanel.add(elem);
    });
    $('.fontDisp').click(function () {
        var elem = new FontElement(new Util().makeid(), 'font', 10, 250, $(this).attr('name'));
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });
    $('.save').click(function () {
        new Save().save();
    });
    $('.itemTextfield').click(function () {
        var elem = new TextfieldElement(new Util().makeid(), 'text', 100, 100, 200, 50);
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });
    $('.itemImg').click(function () {
        var elem = new ImgElement(new Util().makeid(), 'img', 100, 100, 200, 200, IMG + 'logo256.png');
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });
    $('.itemRect').click(function () {
        var elem = new RectElement(new Util().makeid(), 'rect', 100, 100, 200, 50);
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });
    $('.itemPB').click(function () {
        var elem = new ProgressbarElement(new Util().makeid(), 'ProgressBar', 100, 100, 500, 10);
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });
    $('.itemHead').click(function () {
        var elem = new HeadElement(new Util().makeid(), 'head', 100, 100, 100, 100, 'DevQuentin');
        if ($('editor').attr('switcher') === 'workspaceOption') {
            opt.addAndDraw(elem);
        } else {
            workspace.addAndDraw(elem);
        }
        new Util().selected();
    });

    //SLIDE OPT
    $(document).click(function (e) {
        var target = $(e.target);

        if (target.parents(".menuitems").length !== 1 && target.parents("nav").length !== 1) {
            $('.menuitems').each(function () {
                $(this).hide("slide", {direction: "left"}, 100);
            });
        }

        var parentEls = target.parents().map(function () {
            return $(this).prop('tagName');
        }).get().join(", ");


        if (!target.hasClass('element') && !(parentEls.indexOf('OPT') > -1)) {
            if (!target.hasClass('Tdragz') && (!target.hasClass('mCSB_dragger_bar') && !target.hasClass('mCSB_dragger') && !target.hasClass('mCustomScrollBox') && !target.hasClass('mCSB_draggerContainer') && !target.hasClass('mCSB_draggerRail'))) {
                $('.containerOpt').hide("slide", {direction: "right"}, 300);

//				if(!target.hasClass('menuitems') && !target.hasClass('liSetting')){
//
//				}

//				if(target.prop('tagName') != 'INPUT' && target.prop('tagName') != 'TEXTAREA'){
//
//				}

                // $('.focus').each(function() {
                // 	$(this).removeClass('focus');
                // });
            }
        }

        if (!((target.hasClass('element') && target.prop('tagName') === 'INPUT') || target.hasClass('Tdragz'))) {
            $('.Tdragz').remove();
        }

    });


    //ADD OTHERS ELEMENTS

    var trash = new Trash();
    var gotol = new Goto();

    $('body').append('<label class="switchopt"><span class="text">Bloquer le menu option</span><input type="checkbox" id="BLOCKOPT"><span class="slider round"></span></label>');

    $('body').append('<div class="notifContainer"><i class="fa fa-times-circle notifClose" aria-hidden="true"></i><div class="notifContent"></div></div>');
    $('.notifContainer').css('display', 'none');




    $(window).scroll(function (event) {
        $(document).scrollTop(0, 0);
        $(document).scrollLeft(0, 0);
    });

    var upload = new UploadPanel();


    //CLIENT ACTION

    if (STATE == 'PREINIT') {
        if (IDTHEME != 0) {
            var load = new Load();
            load.loadLauncher(IDTHEME);
        } else {
            var setup = new GuiSetup("setup", '.export', "Initialisation", 1200, 600);
            setup.init();
            setup.draw();
        }
    } else if (STATE == 'ACHAT') {
        $('.button').hide();
        $('.faIcon').hide();
        $('.fonts').hide();
        $('.tools').hide();
        $('.save').hide();
        $('.setting').hide();
    } else if (STATE == 'CURRENT') {
        var load = new Load();
        load.loadLauncher();
    }


    // $('.export').click(function(event) {
    // 	document.location.href = SITE + "compilation/step_first/" + ID + "/" + TOKEN;
    // });

    console.log(STATE);


    new Util().selected();
    Listening.registerEvents();


    //SELECTED
    // $('.element').mousedown(function(){
    // 	$('.selected').each(function() {
    // 		$(this).removeClass('selected');
    // 	});
    // 	$('.focus').each(function() {
    // 		$(this).removeClass('focus');
    // 	});
    // 	$(this).addClass('selected');
    // 	if ($('.selected').hasClass('TextfieldElement')) {
    // 		$(".Tdrag").remove();
    // 		if ($('editor').attr('switcher') == 'workspaceOption') {
    // 			$('.workspaceOption').append('<div class="Tdrag" style="z-index: 9999"><i class="fa fa-arrows Tdragz"></i></div>');
    // 		}else{
    // 			$('.workspace').append('<div class="Tdrag" style="z-index: 9999"><i class="fa fa-arrows Tdragz"></i></div>');
    // 		}
    // 		$('.Tdrag').css({
    // 							'position' : 'absolute',
    // 							'left' : $('.selected').position().left,
    // 							'top' : $('.selected').position().top
    // 						});
    // 		$('.Tdrag').draggable();
    // 		$(".Tdrag").bind("drag",function() {
    // 			var offset = $(this).position();
    // 			new OptionPanel().update($('.selected'));
    // 			$('.selected').css({
    // 				'left' : offset.left,
    // 				'top' : offset.top
    // 			});
    // 		});
    // }else {
    // 	$(".Tdrag").remove();
    // }
    // });


    setInterval(function () {
        SOCKET.emit('AUTH', COOKIE);
        if ($('.avgrund-popin ').length < 1) {
            new Save().save();
        }
    }, 300000); //300000
});

