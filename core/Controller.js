/**
 * Created by Anonymous on 09/05/2017.
 */
import Nunjucks from 'nunjucks';

export default class Controller {

    constructor(db) {
        this.theme = 'default';
        this.layout = 'default';
        this.db = db;
        this.vars = [];
    }

    render(req, res, page, data){
        const controller = this.constructor.name.replace('Controller', '').toLowerCase();
        // this.layout = req.session.user ? 'user' : 'default';

        let render = Nunjucks.render(`${this.theme}/${controller}/${page}.html`, data ? data : this.vars);
        res.render(`${this.theme}/layout/${this.layout}.html`, {content: render, flash: req.session.flash, flash_type: req.session.flash_type, user: req.session.user});
        req.session.flash = false;
    }

    setFlash(req, message, type){
        console.log(message);
        req.session.flash = Array.isArray(message) ? message : [message];
        req.session.flash_type = type ? type : 'success';
    }

    extartErrors(errors){
        const keys = Object.keys(errors);
        let flash = [];
        keys.forEach(function (v) {
            flash.push(errors[v].message);
        });
        return flash;
    }

    isLogged(req){
        return !!req.session.user;
    }


}
