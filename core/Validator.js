/**
 * Created by Anonymous on 10/05/2017.
 */
export default class Validator {

    static email(v){
        return /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/.test(v);
    }

    static alphaNum(v){
        return /[a-zA-Z0-9]*/.test(v);
    }
}
