/**
 * Created by Anonymous on 18/05/2017.
 */
import Crypto from 'crypto';
import Fs from 'fs';
import Path from 'path';

export default class Util {

    /**
     * Génére une chaine de caratére aléatoire
     * @param howMany taille
     * @param chars caractère utilisé pour la génération
     * @returns {string}
     */
    static random (howMany, chars) {
        chars = chars || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        let rnd = Crypto.randomBytes(howMany)
            , value = new Array(howMany)
            , len = chars.length;
        for (let i = 0; i < howMany; i++) {
            value[i] = chars[rnd[i] % len]
        }
        return value.join('');
    }

    /**
     * To remove folder Syncronously
     * @param path folder
     */
    static deleteFolderRecursive(path){
        if(Fs.existsSync(path)) {
            Fs.readdirSync(path).forEach(function(file, index){
                let curPath = path + "/" + file;
                if(Fs.lstatSync(curPath).isDirectory()) { // recurse
                    Util.deleteFolderRecursive(curPath);
                } else { // delete file
                    Fs.unlinkSync(curPath);
                }
            });
            Fs.rmdirSync(path);
        }
    }

    /**
     * Normalize \\ paths to / paths.
     * @param filepath
     * @returns {*}
     */
    static unixifyPath(filepath) {
        if (process.platform === 'win32') {
            return filepath.replace(/\\/g, '/');
        } else {
            return filepath;
        }
    }

    /**
     * RGB to hex conversion
     * @param hex
     * @returns {*}
     */
    static hexToRgb(hex) {
        let result = /#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})/i.exec(hex);
        return result ? [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16)
        ] : null;
    }

    static parseCookies (request) {
        let list = {},
            rc = request.headers.cookie;

        rc && rc.split(';').forEach(function( cookie ) {
            let parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });

        return list;
    }

    static addHours(date, hours){
        return date.setTime(date.getTime()+(hours*60*60*1000));
    }
}