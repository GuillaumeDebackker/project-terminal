/**
 * Created by Anonymous on 17/06/2017.
 */
import Fs from 'fs';

export default class Logger {

    constructor(ouput){
        this.ouput = ouput;
        Fs.writeFile(ouput, "====LOGFILLE PASA====\n", (err) => {
            if(err) return console.log("ERROR : fail logfile");
        });
    }

    info(msg){
        console.log('[INFO] ' + msg);
        Fs.appendFile(this.ouput, '[INFO] ' + msg + '\n', (err) => {
            if (err) throw err;
        });
    }

    danger(msg){
        console.log('[WARN] ' + msg);
        Fs.appendFile(this.ouput, '[WARN] ' + msg + '\n', (err) => {
            if (err) throw err;
        });
    }
}