/**
 * Created by Anonymous on 10/05/2017.
 */
/**
 * Created by Anonymous on 27/04/2017.
 */
import Mongoose from 'mongoose';
import Validator from '../core/Validator'

const User = Mongoose.Schema({
    username: {
        type: String,
        unique: [true, 'Username existant déjà'],
        required: [true, 'Le champ username n\'est pas défini'],
        min: [5, 'Pseudo trop court'],
        validate: {
            validator: function (v) {
                return /[a-zA-Z0-9]*/.test(v);
            },
            message: '{VALUE} n\'est pas de type alpha numérique'
        }
    },
    password: {
        type: String,
        required: [true, 'Le champ password n\'est pas défini'],
        min: [7, 'Password trop court'],
    },
    role: String,
    email: {
        type: String,
        unique: true,
        required: [true, 'Le champ email n\'est pas défini'],
        validate: {
            validator: function (v) {
                return /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/.test(v);
            },
            message: '{VALUE} n\'est pas de type alpha numérique'
        }
    },
});
module.exports = User;